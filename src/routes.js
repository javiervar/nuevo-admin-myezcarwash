import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const DashboardDefault = React.lazy(() => import('./Pages/Dashboard/Default'));
const Charts =React.lazy(() => import('./Pages/Charts/Nvd3Chart/index'));
//pages
const Reports = React.lazy(() => import('./Pages/Reports/Reports'));
const Reservations = React.lazy(() => import('./Pages/Reservations/Reservations'));
const ReservationDetail = React.lazy(() => import('./Pages/Reservations/ReservationDetail'));
const PayReservation = React.lazy(() => import('./Pages/Reservations/PayReservation'));

const Retailers = React.lazy(() => import('./Pages/Retailers/Retailers'));
const RetailerDetail = React.lazy(() => import('./Pages/Retailers/RetailerDetails'));
const AddRetailer = React.lazy(() => import('./Pages/Retailers/AddRetailer'));

const Employees = React.lazy(() => import('./Pages/Employees/Employees'));
const EmployeeDetail = React.lazy(() => import('./Pages/Employees/EmployeeDetail'));
const AddEmployee = React.lazy(() => import('./Pages/Employees/AddEmployee'));

const Stationary = React.lazy(() => import('./Pages/Locations/Stationary'));
const Mobile = React.lazy(() => import('./Pages/Locations/Mobile'));
const AddLocations = React.lazy(() => import('./Pages/Locations/AddLocation'));
const EditLocation = React.lazy(() => import('./Pages/Locations/EditLocation'));
const EditMobile = React.lazy(() => import('./Pages/Locations/EditMobile'));
const EditDrawing = React.lazy(() => import('./Pages/Locations/EditDrawingMap'));

const AddMobile = React.lazy(() => import('./Pages/Locations/AddMobile'));

const Clients = React.lazy(() => import('./Pages/Clients/Clients'));
const ClientDetail = React.lazy(() => import('./Pages/Clients/ClientDetail'));
const UserEdit = React.lazy(() => import('./Pages/Clients/UserEdit'));

const Services = React.lazy(() => import('./Pages/Services/Services'));
const Additionals = React.lazy(() => import('./Pages/Additionals/Additionals'));
const AddAdditional = React.lazy(() => import('./Pages/Additionals/AddAdditional'));
const Promo = React.lazy(() => import('./Pages/Promo/Promo'));
const AddPromo = React.lazy(() => import('./Pages/Promo/AddPromo'));
const EditPromo = React.lazy(() => import('./Pages/Promo/EditPromo'));

const routes = [
    { path: '/home', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/stats', exact: true, name: 'Stats', component: Charts },

    { path: '/reports', exact: true, name: 'Reports', component: Reports },
    { path: '/reservations', exact: true, name: 'Reservations', component: Reservations },
    { path: '/reservations/detail', exact: true, name: 'ReservationDetail', component: ReservationDetail },
    { path: '/reservations/pay', exact: true, name: 'ReservationPay', component: PayReservation },

    { path: '/retailers', exact: true, name: 'Retailers', component: Retailers },
    { path: '/retailers/detail', exact: true, name: 'RetailerDetail', component: RetailerDetail },
    { path: '/retailers/add', exact: true, name: 'AddRetailer', component: AddRetailer },

    { path: '/stationary', exact: true, name: 'Locations', component: Stationary },
    { path: '/mobile', exact: true, name: 'Locations', component: Mobile },
    { path: '/locations/add', exact: true, name: 'AddLocations', component: AddLocations },
    { path: '/locations/editLocation', exact: true, name: 'EditLocation', component: EditLocation },
    { path: '/locations/editMobile', exact: true, name: 'EditMobile', component: EditMobile },
    { path: '/locations/editDrawing', exact: true, name: 'EditDrawing', component: EditDrawing },
    { path: '/locations/addMobile', exact: true, name: 'AddMobile', component: AddMobile },
    { path: '/employees', exact: true, name: 'Employees', component: Employees },
    { path: '/employees/detail', exact: true, name: 'EmployeeDetail', component: EmployeeDetail },
    { path: '/employees/add', exact: true, name: 'AddEmployee', component: AddEmployee },

    { path: '/clients', exact: true, name: 'Clients', component: Clients },
    { path: '/clients/detail', exact: true, name: 'ClientsDetail', component: ClientDetail },
    { path: '/user/edit', exact: true, name: 'EditUser', component: UserEdit },
    { path: '/services', exact: true, name: 'Services', component: Services },
    { path: '/adds', exact: true, name: 'Additionals', component: Additionals },
    { path: '/adds/create', exact: true, name: 'AddAdditional', component: AddAdditional },
    { path: '/promos', exact: true, name: 'Promo', component: Promo },
    { path: '/promos/edit', exact: true, name: 'EditPromo', component: EditPromo },
    { path: '/promos/create', exact: true, name: 'AddPromo', component: AddPromo },
   
];

export default routes;