import React, { Component, Suspense } from 'react';
import { Switch, Route,HashRouter } from 'react-router-dom';
import Loadable from 'react-loadable';

import '../../node_modules/font-awesome/scss/font-awesome.scss';

import Loader from './layout/Loader'
import Aux from "../hoc/_Aux";
import ScrollToTop from './layout/ScrollToTop';
import routes from "../route";
import staticData from "../store/staticData";
import Loading from '../Pages/UIElements/Basic/Loading';

const AdminLayout = Loadable({
    loader: () => import('./layout/AdminLayout'),
    loading: Loader
});

class App extends Component {

    state = {
        user: {}
    }

    componentWillMount() {

        if (this.props.user !== undefined) {
            this.setState({
                user: this.props.user
            })
        }
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
        }
    }

    componentDidUpdate() {
        console.log("sup->", localStorage.getItem("adminwash"));
        if (localStorage.getItem("adminwash") !== null && this.state.user === {}) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
        }
    }

    render() {
        const menu = routes.map((route, index) => {
            return (route.component) ? (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                        <route.component {...props} />
                    )} />
            ) : (null);
        });

        

        

        return (
            <Aux>
                <ScrollToTop>
                    <Suspense fallback={<Loader />}>
                        
                        <Switch>
                            {menu}
                                <Route
                                    path="/"
                                    render={(props) => <AdminLayout {...props} isAuthed={true} user={this.state.user} />}

                                />
                                
                        </Switch>
                       

                    </Suspense>
                </ScrollToTop>
            </Aux>
                    );
                }
            }
            
            export default App;
