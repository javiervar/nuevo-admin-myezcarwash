import React, { Component } from 'react';
import { Dropdown, Form } from 'react-bootstrap';

import ChatList from './ChatList';
import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";

import Avatar1 from '../../../../../assets/images/user/avatar-1.jpg';
import Avatar2 from '../../../../../assets/images/user/avatar-2.jpg';
import Avatar3 from '../../../../../assets/images/user/avatar-3.jpg';
import i18n from '../../../../../i18n';
import { withNamespaces } from 'react-i18next';
import { Redirect } from 'react-router'
import {
    withRouter
} from 'react-router-dom'
class NavRight extends Component {
    state = {
        listOpen: false,
        user: {},
        language:[1]
    };

    
    componentDidMount() {
        var userLang = navigator.language || navigator.userLanguage;
        if (userLang == "es-MX") {
            i18n.changeLanguage('es');
            this.setState({
                language:[2]
            })
        }

    }

    logOut = () => {
        console.log("log out")
        localStorage.removeItem('adminwash');
        window.location.replace("/");

    }

    changeLanguage = (val) => {
        var language = val.target.value;
        console.log(language);
        if (language == 1) {
            i18n.changeLanguage('en');
        } else {
            i18n.changeLanguage('es');
        }
        this.props.history.push("/home")
    }

    render() {
        const { t } = this.props;
        return (
            <Aux>
                <ul className="navbar-nav ml-auto">
                    <li>
                        <Dropdown alignRight={!this.props.rtlLayout}>
                            <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                                <i className="icon feather icon-bell" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu alignRight className="notification">
                                <div className="noti-head">
                                    <h6 className="d-inline-block m-b-0">Notifications</h6>
                                    <div className="float-right">
                                        <a href={DEMO.BLANK_LINK} className="m-r-10">mark as read</a>
                                        <a href={DEMO.BLANK_LINK}>clear all</a>
                                    </div>
                                </div>
                                <ul className="noti-body">
                                    {/** 
                                    <li className="n-title">
                                        <p className="m-b-0">NEW</p>
                                    </li>
                                    <li className="notification">
                                        <div className="media">
                                            <img className="img-radius" src={Avatar1} alt="Generic placeholder"/>
                                            <div className="media-body">
                                                <p><strong>John Doe</strong><span className="n-time text-muted"><i
                                                    className="icon feather icon-clock m-r-10"/>30 min</span></p>
                                                <p>New ticket Added</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="n-title">
                                        <p className="m-b-0">EARLIER</p>
                                    </li>
                                    <li className="notification">
                                        <div className="media">
                                            <img className="img-radius" src={Avatar2} alt="Generic placeholder"/>
                                            <div className="media-body">
                                                <p><strong>Joseph William</strong><span className="n-time text-muted"><i
                                                    className="icon feather icon-clock m-r-10"/>30 min</span></p>
                                                <p>Prchace New Theme and make payment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="notification">
                                        <div className="media">
                                            <img className="img-radius" src={Avatar3} alt="Generic placeholder"/>
                                            <div className="media-body">
                                                <p><strong>Sara Soudein</strong><span className="n-time text-muted"><i
                                                    className="icon feather icon-clock m-r-10"/>30 min</span></p>
                                                <p>currently login</p>
                                            </div>
                                        </div>
                                    </li>
                                */}
                                </ul>
                                <div className="noti-footer">
                                    <a href={DEMO.BLANK_LINK}>show all</a>
                                </div>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                    {/** 
                    <li className={this.props.rtlLayout ? 'm-r-15' : 'm-l-15'}>
                        <a href={DEMO.BLANK_LINK} className="displayChatbox" onClick={() => {this.setState({listOpen: true});}}><i className="icon feather icon-mail"/></a>
                    </li>
                    */}

                    <li>
                        <Dropdown alignRight={!this.props.rtlLayout} className="drp-user">
                            <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                                <i className="icon feather icon-settings" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu alignRight className="profile-notification">
                                <div className="pro-head">
                                    <img src={Avatar2} className="img-radius" alt="User Profile" />
                                    <span>{this.props.user.user_name}</span>

                                </div>
                                <ul className="pro-body">
                                    <li>
                                        <div className="dropdown-item" style={{ display: "flex" }}>
                                            <i className="fas fa-globe-americas" style={{ alignSelf: "center", color: "#888" }} />
                                            <Form.Control defaultValue={this.state.language}  as="select" size="sm" style={{ height: 30, padding: 0, marginLeft: 10 }} onChange={this.changeLanguage}>
                                                <option value="1">English</option>
                                                <option value="2">Spanish</option>
                                            </Form.Control>
                                        </div>
                                    </li>
                                    <li><a href={DEMO.BLANK_LINK} className="dropdown-item"><i className="feather icon-settings" /> {t('Settings')}</a></li>
                                    <li><a href={DEMO.BLANK_LINK} className="dropdown-item"><i className="feather icon-user" /> {t('Profile')}</a></li>

                                    <li>
                                        <a href={DEMO.BLANK_LINK} className="dropdown-item" title="Logout" onClick={this.logOut}>
                                            <i className="feather icon-log-out" />
                                            {t('Log out')}
                                        </a>
                                    </li>
                                </ul>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                </ul>
                <ChatList listOpen={this.state.listOpen} closed={() => { this.setState({ listOpen: false }); }} />
            </Aux>
        );
    }
}

export default withNamespaces()(withRouter(NavRight));
