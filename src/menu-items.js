
var menu = {
    items_en: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'Home',
                    title: 'Home',
                    type: 'item',
                    url: '/home',
                    icon: 'feather icon-home',
                },
                {
                    id: 'Stats',
                    title: 'Analytics',
                    type: 'item',
                    url: '/stats',
                    icon: 'fas fa-chart-line',
                },
                {
                    id: 'Reports',
                    title: 'Reports',
                    type: 'item',
                    url: '/reports',
                    icon: 'feather icon-clipboard',
                },
                {
                    id: 'Reservations',
                    title: 'Reservations',
                    type: 'item',
                    url: '/reservations',
                    icon: 'far fa-calendar-alt',
                },
                {
                    id: 'promo',
                    title: 'Promo code',
                    type: 'item',
                    url: '/promos',
                    icon: 'fas fa-percent',
                },
            ]
        },

        {
            id: 'users',
            title: 'Users',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'retailers',
                    title: 'Retailers',
                    type: 'item',
                    url: '/retailers',
                    icon: 'fas fa-user-tie',
                },
                {
                    id: 'Employees',
                    title: 'Employees',
                    type: 'item',
                    url: '/employees',
                    icon: 'feather icon-user',
                },
                {
                    id: 'breadcrumb-pagination',
                    title: 'Clients',
                    type: 'item',
                    url: '/clients',
                    icon: 'feather icon-user',
                },

            ]


        },
        {
            id: 'service',
            title: 'Service',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'stationary',
                    title: 'Stationary',
                    type: 'item',
                    url: '/stationary',
                    icon: 'fas fa-map-marker-alt',
                },
                {
                    id: 'mobile',
                    title: 'Mobile',
                    type: 'item',
                    url: '/mobile',
                    icon: 'fas fa-car',
                },
                {
                    id: 'services',
                    title: 'Services',
                    type: 'item',
                    url: '/services',
                    icon: 'fas fa-car',
                },
                {
                    id: 'adds-services',
                    title: 'Adds',
                    type: 'item',
                    url: '/adds',
                    icon: 'far fa-plus-square',
                },


            ]


        },
    ],
    items_es: [
        {
            id: 'navigation',
            title: 'Navegación',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'Home',
                    title: 'Inicio',
                    type: 'item',
                    url: '/home',
                    icon: 'feather icon-home',
                },
                {
                    id: 'Stats',
                    title: 'Analítica',
                    type: 'item',
                    url: '/stats',
                    icon: 'fas fa-chart-line',
                },
                {
                    id: 'Reports',
                    title: 'Reportes',
                    type: 'item',
                    url: '/reports',
                    icon: 'feather icon-clipboard',
                },
                {
                    id: 'Reservations',
                    title: 'Reservaciones',
                    type: 'item',
                    url: '/reservations',
                    icon: 'far fa-calendar-alt',
                },
                {
                    id: 'promo',
                    title: 'Código promocional',
                    type: 'item',
                    url: '/promos',
                    icon: 'fas fa-percent',
                },
            ]
        },
        {
            id: 'users',
            title: 'Usuarios',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'retailers',
                    title: 'Minoristas',
                    type: 'item',
                    url: '/retailers',
                    icon: 'fas fa-user-tie',
                },
                {
                    id: 'Employees',
                    title: 'Empleados',
                    type: 'item',
                    url: '/employees',
                    icon: 'feather icon-user',
                },
                {
                    id: 'breadcrumb-pagination',
                    title: 'Clientes',
                    type: 'item',
                    url: '/clients',
                    icon: 'feather icon-user',
                },

            ]


        },
        {
            id: 'service',
            title: 'Servicio',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'stationary',
                    title: 'Estacionario',
                    type: 'item',
                    url: '/stationary',
                    icon: 'fas fa-map-marker-alt',
                },
                {
                    id: 'mobile',
                    title: 'Móvil',
                    type: 'item',
                    url: '/mobile',
                    icon: 'fas fa-car',
                },
                {
                    id: 'services',
                    title: 'Servicios',
                    type: 'item',
                    url: '/services',
                    icon: 'fas fa-car',
                },
                {
                    id: 'adds-services',
                    title: 'Adds',
                    type: 'item',
                    url: '/adds',
                    icon: 'far fa-plus-square',
                },

            ]

        },

    ]

}


export default menu;