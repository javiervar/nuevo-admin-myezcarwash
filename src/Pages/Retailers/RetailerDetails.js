import React from 'react';
import { Row, Col, Card, Modal, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from '../Reservations/Components/TableComponent';
import TableComponentRetailer from '../Employees/Components/TableComponentRetailer'
import { Link } from 'react-router-dom'
import ListRetailers from '../Employees/Components/ListRetailers';

import CONSTANT from '../../store/constant';
import '../Reservations/Reports.css'
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';

class RetailerDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            loading: true,
            loadingHistory: true,
            loadingRetailers: false,
            loadingOtherRet: false,
            othersRetailers: [],
            employees: [],
            info: [],
            pages: 0,
            currentPage: 1,
            searchBy: 0,
            searviceType: 1,
            dateFrom: 0,
            dateTo: 0,
            term: 0,
            retailers: [],
            status: -1,
            historyData: [],
            historyAdds: []
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.getInfo();
        this.getHistoryReservations(0);
        this.getEmployeeRetailers();
        this.getRetailers();
        this.getNotRetailers();

    }
    getInfo = () => {
        this.setState({ loading: true })
        var postData = {
            params: {
                user_id: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getUserGeneralInfo", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loading: false,
                    info: response.data.data[0],

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getEmployeeRetailers = () => {
        this.setState({ loadingRetailers: true })
        var postData = {
            params: {
                retailer: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getEmployeesByRetailers", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingRetailers: false,
                    employees: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getRetailers = () => {
        this.setState({ loadingRetailers: true })
        var postData = {
            params: {
                employee: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getEmployeeRetailers", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingRetailers: false,
                    retailers: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getHistoryReservations = (page) => {
        this.setState({ loadingHistory: true, currentPage: page });
        const { searviceType, searchBy, dateFrom, dateTo, term, status } = this.state;
        var option = 1;
        var dateToTemp = dateTo;
        if (dateFrom != 0) {
            option = 2;
            if (dateToTemp == 0) {
                dateToTemp = new Date().toJSON().slice(0, 10);
            }
        }
        var postData = {
            params: {
                employeeId: -1,
                opc: option,
                from: dateFrom,
                to: dateToTemp,
                service: searviceType,
                page: page,
                by: searchBy,
                txt: term,
                status: -1,
                retailer: this.props.location.state[0].id
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllReservations", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingHistory: false,
                    historyData: response.data.data,
                    historyAdds: response.data.adds,
                    pages: response.data.pages
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };


    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }

    assign = (id) => {
        console.log(id)
        var params = {
            employee: this.props.location.state[0].id,
            retailer: id
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/assignEmployee", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("This employee was assign successfully to retailer #" + id);
                    this.setState({
                        loadingAssign: false,
                    }, () => {
                        this.handleModal();
                        this.getEmployeeRetailers();
                        this.getRetailers();
                        this.getNotRetailers();
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loadingAssign: false,
                    });
                }
            });
    }

    revoke = (id) => {
        console.log(id)
        var params = {
            employee: this.props.location.state[0].id,
            retailer: id
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/revokeEmployee", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("This employee was revoke successfully to retailer #" + id);
                    this.setState({
                        loadingAssign: false,
                    }, () => {
                        this.handleModal();
                        this.getEmployeeRetailers();
                        this.getNotRetailers();
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loadingAssign: false,
                    });
                }
            });
    }

    getNotRetailers = () => {
        this.setState({ loadingOtherRet: true })
        var postData = {
            params: {
                employee: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getNotRetailers", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingOtherRet: false,
                    othersRetailers: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
        const { info } = this.state;
        const {t}=this.props;
        return (
            <Aux>
                <Modal show={this.state.showModal} onHide={this.handleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{t("Assign to")}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.loadingAssign ? (
                            <Loading />
                        ) : (
                                <ListRetailers data={this.state.othersRetailers} handleOnClick={this.assign} />

                            )}
                    </Modal.Body>
                    <Modal.Footer>

                        <Button variant="secondary" onClick={this.handleModal}>
                            {t("Close")}
                        </Button>

                    </Modal.Footer>
                </Modal>
                <Row>
                    <Col>
                        {this.state.loading ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("User")} # {info.USER_ID}</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Row style={{ paddingBottom: 5, borderBottom: "1px solid #ddd" }}>
                                            <Col md={12}>
                                                <h5>{info.US_TYPE}</h5>
                                            </Col>
                                            <Col md={6}>
                                                <p>
                                                    <b>{t("User name")}: </b>{info.USER_NAME}
                                                </p>
                                                <p>
                                                    <b>{t("Email")}: </b>{info.EMAIL}
                                                </p>
                                                <p>
                                                    <b>{t("Password")}: </b>{info.PASS}
                                                </p>
                                                <p>
                                                    <b>{t("Name")}: </b>{info.NAME} {info.LAST_NAME}
                                                </p>
                                                <p>
                                                    <b>{t("Phone")}: </b>{info.PHONE}
                                                </p>

                                            </Col>

                                        </Row>

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                    <Col>
                        {this.state.loadingRetailers ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("Employees")}</Card.Title>
                                        <br></br>

                                        <Link
                                            className="btn btn-info btn-sm"
                                            style={{ marginTop: 18 }}
                                            to={{
                                                pathname: "/employees/add",
                                                state: [{ id: this.props.location.state[0].id }]
                                            }}
                                        >{t("Add")}</Link>
                                    </Card.Header>
                                    <Card.Body>
                                        {this.state.employees.map((employee) => {
                                            return (
                                                <p>{employee.USER_NAME}</p>
                                            )
                                        })}
                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.state.loadingRetailers ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("Assign as employee")}</Card.Title>
                                        <br></br>
                                        <Button variant="info" size="sm" style={{ marginTop: 18 }} onClick={this.handleModal}>
                                           {t("Assign to")}
                                        </Button>
                                    </Card.Header>
                                    <Card.Body>
                                        {this.state.loadingAssign ? (
                                            <Loading />
                                        ) : (
                                                this.state.retailers.length == 0 ? (
                                                    <div className='noResults'>
                                                        <span>{t("No results")}</span>
                                                    </div>
                                                ) : (
                                                        <TableComponentRetailer data={this.state.retailers} handleOnClick={this.revoke} />
                                                    )
                                            )}

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.state.loadingHistory ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("Booking history")}</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        {this.state.historyData.length > 0 ? (
                                            <div>
                                                <TableComponent data={this.state.historyData} adds={this.state.historyAdds} />

                                                <ReactPaginate
                                                    previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                                    nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}

                                                    forcePage={this.state.currentPage}
                                                    pageCount={this.state.pages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={5}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </div>
                                        ) : (
                                                <div className='noResults'>
                                                    <span>{t("No results")}</span>
                                                </div>
                                            )}

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(RetailerDetails);