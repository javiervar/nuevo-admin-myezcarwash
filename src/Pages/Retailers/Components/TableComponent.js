import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const {t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/retailers/detail",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{t("Details")}</Link>
                    </td>
                    <th scope="row">{row.USER_ID}</th>
                    <td>{row.USER_NAME}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.PASS}</td>
                    <td>{row.NAME}</td>
                    <td>{row.LAST_NAME}</td>
                    <td>{row.COMPANY_NAME}</td>
                    <td>{row.OFFICE_PHONE1}</td>
                    <td>{row.OFFICE_PHONE2}</td>
                    <td>{row.PHONE}</td>
                    <td>{row.COUNTRY}</td>
                    <td>{row.STATE}</td>
                    <td>{row.CITY}</td>
                    <td>{row.STREET}</td>
                    <td>{row.NO}</td>
                    <td>{row.ZIP}</td>


                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>{t("Command")}</th>
                        <th>Id</th>
                        <th>{t("User Name")}</th>
                        <th>{t("Email")}</th>
                        <th>{t("Password")}</th>
                        <th>{t("Name")}</th>
                        <th>{t("Last Name")}</th>
                        <th>{t("Company")}</th>
                        <th>{t("Office phone")} 1</th>
                        <th>{t("Office phone")} 2</th>
                        <th>{t("Direct phone")}</th>
                        <th>{t("Country")}</th>
                        <th>{t("State")}</th>
                        <th>{t("City")}</th>
                        <th>{t("Street")}</th>
                        <th>No</th>
                        <th>{t("ZIP")}</th>

                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);