import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';

class AddRetailer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            user: {},
            userName: "",
            userType: 2,
            firstName: "",
            lastName: "",
            pass: "",
            companyName: "",
            officePhone: "",
            officePhone2: "",
            fax: "",
            directPhone: "",
            email: "",
            street: "",
            num: "",
            city: "",
            zip: "",
            state: "",
            country: ""




        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
    }


    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });

        var params = this.state
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addRetailer", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("New retailer was added successfully");
                    this.setState({
                        loading: false,
                        userName: "",
                        userType: 2,
                        firstName: "",
                        lastName: "",
                        pass: "",
                        companyName: "",
                        officePhone: "",
                        officePhone2: "",
                        fax: "",
                        directPhone: "",
                        email: "",
                        street: "",
                        num: "",
                        city: "",
                        zip: "",
                        state: "",
                        country: ""
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loading: false,
                    });
                }
            });
    }




    render() {
        const{t}=this.props;
        return (
            <Aux>
                <Row>
                    <Col>


                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Add")} {t("Retailer")}</Card.Title>

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Form onSubmit={this.onFormSubmit}>

                                            <Row>
                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Label>{t("User Name")}</Form.Label>
                                                        <Form.Control type="Text" name="userName"
                                                            value={this.state.userName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        userName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />

                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Password")} </Form.Label>
                                                        <Form.Control type="text" name="pass"
                                                            value={this.state.pass}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        pass: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("First Name")}:</Form.Label>
                                                        <Form.Control type="text" name="firstName"
                                                            value={this.state.firstName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        firstName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Last Name")}:</Form.Label>
                                                        <Form.Control type="text" name="lastName"
                                                            value={this.state.lastName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        lastName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Company name")}:</Form.Label>
                                                        <Form.Control type="text" name="companyName"
                                                            value={this.state.companyName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        companyName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Office phone")}:</Form.Label>
                                                        <Form.Control type="text" name="officePhone"
                                                            value={this.state.officePhone}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        officePhone: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Office phone")} 2 ({t("optional")}):</Form.Label>
                                                        <Form.Control type="text" name="officePhone2"
                                                            value={this.state.officePhone2}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        officePhone2: e.target.value
                                                                    })
                                                            }}

                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>Fax ({t("optional")}):</Form.Label>
                                                        <Form.Control type="text" name="fax"
                                                            value={this.state.fax}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        fax: e.target.value
                                                                    })
                                                            }}

                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Direct phone")}:</Form.Label>
                                                        <Form.Control type="text" name="directPhone"
                                                            value={this.state.directPhone}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        directPhone: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Email")}:</Form.Label>
                                                        <Form.Control type="email" name="email"
                                                            value={this.state.email}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        email: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                            </Row>
                                            <Row>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Street")}:</Form.Label>
                                                        <Form.Control type="text" name="street"
                                                            value={this.state.street}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        street: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>No:</Form.Label>
                                                        <Form.Control type="text" name="num"
                                                            value={this.state.num}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        num: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("City")}:</Form.Label>
                                                        <Form.Control type="text" name="city"
                                                            value={this.state.city}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        city: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("State")}:</Form.Label>
                                                        <Form.Control type="text" name="state"
                                                            value={this.state.state}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        state: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>

                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("ZIP")}:</Form.Label>
                                                        <Form.Control type="text" name="zip"
                                                            value={this.state.zip}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        zip: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>

                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Country")}:</Form.Label>
                                                        <Form.Control type="text" name="country"
                                                            value={this.state.country}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        country: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>
                                            </Row>

                                            <Button variant="primary" type="submit">
                                                {t("Submit")}
                                            </Button>
                                        </Form>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(AddRetailer);