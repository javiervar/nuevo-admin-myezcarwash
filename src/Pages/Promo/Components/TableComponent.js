import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const { t } = this.props;

        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <th>
                        <Link
                            className="btn btn-info btn-sm"
                            to={{
                                pathname: "/promos/edit",
                                state: [{ id: row.ID }]
                            }}
                        >{t("Edit")}</Link>
                    </th>
                    <th scope="row">{row.ID}</th>
                    <td>{row.CODE}</td>
                    <td>{row.START}</td>
                    <td>{row.FINISH}</td>
                    <td>{row.DISCOUNT}</td>
                    <td>{row.USE_LIMIT}</td>
                    <td>
                        {row.STATUS == 1 ? 'Active' : 'Disabled'}
                    </td>


                </tr>
            );
        })

        return rows


    }
    render() {
        const { t } = this.props;

        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">
                    <tr>
                    <th>{t("Command")}</th>
                        <th>Id</th>
                        <th>{t("Code")}</th>
                        <th>{t("Date start")}</th>
                        <th>{t("Date finish")}</th>
                        <th>{t("Discount")} %</th>
                        <th>{t("Use limit")}</th>
                        <th>{t("Status")}</th>
                        
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);