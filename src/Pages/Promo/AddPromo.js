import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class AddPromo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            promo: '',
            discount: 0,
            limit: 1,
            startDate: null,
            finishDate: null,
            user:{}

        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user=JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user:user
            })
            console.log(user)
        }else{
            this.props.history.push("/");
        } 
       
    }

    componentDidMount() {
    }


    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        const { promo, discount, limit, startDate, finishDate } = this.state;
        console.log(promo, discount, limit, startDate, finishDate);
        var params = {
            promo: promo,
            discount: discount,
            limit: limit,
            startDate: startDate,
            finishDate: finishDate
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addPromo", params)
            .then((response)=> {
                console.log(response)
                if(response.data.error==0){
                alert("The promo code was added successfully");
                this.setState({
                    loading:false,
                    promo: '',
                    discount: 0,
                    limit: 1,
                    startDate: Date.now(),
                    finishDate: Date.now(),
                })
            }else{
                alert(response.data.text)
                this.setState({
                    loading:false,
                });
            }
            });
    }




    render() {
        const { t } = this.props;

        return (
            <Aux>
                <Row>
                    <Col>


                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Add new promo")}</Card.Title>

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Form onSubmit={this.onFormSubmit}>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>{t("Promo code")}</Form.Label>
                                                <Form.Control type="Text" name="promo"
                                                    value={this.state.promo}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                promo: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />

                                            </Form.Group>

                                            <Form.Group controlId="formBasicPassword">
                                                <Form.Label>% {t("Discount")} </Form.Label>
                                                <Form.Control type="number" name="discount"
                                                    value={this.state.discount}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                discount: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPassword">
                                                <Form.Label>{t("Start")} </Form.Label>
                                                <Form.Control type="date" name="startDate"
                                                    value={this.state.startDate}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                startDate: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPassword">
                                                <Form.Label>{t("Finish")} </Form.Label>
                                                <Form.Control type="date" name="finishDate"
                                                    value={this.state.finishDate}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                finishDate: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPassword">
                                                <Form.Label>{t("Use limit")} </Form.Label>
                                                <Form.Control type="number" name="limit"
                                                    value={this.state.limit}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                limit: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Button variant="primary" type="submit">
                                                {t("Submit")}
  </Button>
                                        </Form>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(AddPromo);