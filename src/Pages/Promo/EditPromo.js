import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class EditPromo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            promo: '',
            discount: 0,
            limit: 1,
            startDate: '',
            finishDate: '',
            user: {}

        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)

        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.getInfo();
    }

    getInfo() {
        this.setState({ loading: true })
        var postData = {
            params: {
                promo: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getPromo", postData)
            .then((response) => {
                console.log(response);
                var data = response.data.data[0];
                this.setState({
                    loading: false,
                    promo: data.CODE,
                    discount: data.DISCOUNT,
                    limit: data.USE_LIMIT,
                    startDate: data.START,
                    finishDate: data.FINISH
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }


    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        const { promo, discount, limit, startDate, finishDate } = this.state;
        console.log(promo, discount, limit, startDate, finishDate);
        var params = {
            id: this.props.location.state[0].id,
            promo: promo,
            discount: discount,
            limit: limit,
            startDate: startDate,
            finishDate: finishDate
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/editPromo", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("The promo code was updated successfully");
                    this.setState({
                        loading: false,
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loading: false,
                    });
                }
            });
    }




    render() {
        const { t } = this.props;

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Edit promo code")}</Card.Title>

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Form onSubmit={this.onFormSubmit}>

                                            <Row>
                                                <Col md={6} xs={12}>

                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Label>{t("Promo code")}</Form.Label>
                                                        <Form.Control size="sm" type="Text" name="promo"
                                                            value={this.state.promo}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        promo: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />

                                                    </Form.Group>

                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>% {t("Discount")} </Form.Label>
                                                        <Form.Control size="sm" type="number" name="discount"
                                                            value={this.state.discount}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        discount: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>

                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Start")} </Form.Label>
                                                        <Form.Control size="sm" type="date" name="startDate"
                                                            value={this.state.startDate}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        startDate: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>
                                                <Col md={6} xs={12}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Finish")} </Form.Label>
                                                        <Form.Control size="sm" type="date" name="finishDate"
                                                            value={this.state.finishDate}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        finishDate: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>

                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Use limit")} </Form.Label>
                                                        <Form.Control size="sm" type="number" name="limit"
                                                            value={this.state.limit}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        limit: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                    
                                                </Col>
                                            </Row>


                                            <Button variant="primary" type="submit">
                                                {t("Submit")}
                                            </Button>
                                        </Form>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(EditPromo);