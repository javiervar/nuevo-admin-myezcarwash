import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class Promo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            pages: 0,
            user: {},
            term:''
        }
    }
    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.searchData(0, 3);

    }
    searchData = (page) => {
        console.log("Search");
        this.setState({ loading: true, currentPage: page })
        var postData = {
            params: {
                term: this.state.term,
                page: page,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllPromo", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loading: false,
                    data: response.data.data,
                    pages: response.data.pages

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };

    handleModal = val => {
        console.log(val)
    }




    render() {
        const { t } = this.props;

        return (
            <Aux>
                
                <Row>

                    <Col>
                        <Card>

                            <Card.Body>
                                <Row>

                                    <Col xs={8}>
                                        <Form.Control
                                            type="text"
                                            placeholder="Search"
                                            style={{ marginTop: 18 }}
                                            onChange={(e) => { this.setState({ term: e.target.value }) }}
                                        />
                                    </Col>
                                    <Col>
                                        <Button variant="info" style={{ marginTop: 18 }} onClick={() => this.searchData(0)}>
                                            <span className="fas fa-search"></span>
                                        </Button>
                                    </Col>

                                </Row>


                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Promo codes")}</Card.Title>
                                <Row style={{ marginTop: 10 }}>
                                    <Col>
                                        <Link
                                            className="btn btn-success btn-sm"
                                            to={{
                                                pathname: "/promos/create",

                                            }}
                                        > {t("Add")}  <i className="fas fa-plus"></i></Link>
                                    </Col>
                                </Row>
                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (this.state.data.length > 0 ? (
                                    <div>

                                        <TableComponent data={this.state.data} adds={this.state.adds} onclick={this.handleModal} />

                                        <ReactPaginate
                                            previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                            nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}

                                            forcePage={this.state.currentPage}
                                            pageCount={this.state.pages}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={'pagination'}
                                            subContainerClassName={'pages pagination'}
                                            activeClassName={'active'}
                                        />
                                    </div>) : (
                                        <div className='noResults'>
                                            <span>{t("No results")}</span>
                                        </div>
                                    )
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(Promo);