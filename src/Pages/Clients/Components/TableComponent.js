import React from 'react';
import { Table,Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const {t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/clients/detail",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{t("Detail")}</Link>
                        <Link
                            className="btn btn-warning btn-sm"
                            to={{
                                pathname: "/user/edit",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{t("Edit")}</Link>
                        <Button size="sm" variant="danger" onClick={()=>this.props.handleDelete(row.USER_ID)}>
                            {t("Delete")}
                        </Button>
                    </td>
                    <th scope="row">{row.USER_ID}</th>
                    <td>{row.USER_NAME}</td>
                    <td>{row.PASS}</td>
                    <td>{row.NAME}</td>
                    <td>{row.LAST_NAME}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.PHONE}</td>
                    <td>{row.STATUS == 1 ? 'Active' : 'Disable'}</td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const {t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                    <th>{t("Command")}</th>
                        <th>Id</th>
                        <th>{t("User Name")}</th>
                        <th>{t("Password")}</th>
                        <th>{t("Name")}</th>
                        <th>{t("Last name")}</th>
                        <th>{t("Email")}</th>
                        <th>{t("Phone")}</th>
                        <th>{t("Status")}</th>
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);