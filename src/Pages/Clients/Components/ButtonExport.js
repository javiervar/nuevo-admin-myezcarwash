import React from 'react';
import axios from 'axios';
import { Button, Modal } from 'react-bootstrap';
import LoadingScreen from '../../UIElements/Basic/LoadingScreen';
import ReactExport from 'react-data-export';
import CONSTANT from '../../../store/constant';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

class ButtonExport extends React.Component {
    state = { show: false, loading: false, multiDataSet: [] };
    handleModal = () => {
        this.setState({ show: !this.state.show });
    }
    generateDataSet = () => {
        this.setState({ loading: true });
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRlIjoiMjAxOVwvMDZcLzEyIiwidHlwZSI6IjMiLCJpZCI6IjMyIiwiZW1haWwiOiJlZGdhci52YWxkZXpyQGdtYWlsLmNvbSJ9.7R_ekvoTF2TtwFhz4V7qKOOxLLZHxigLw1FiCNPRJ3s';
        var data = {
            params: {
                page: 0,
                txt: this.props.term,
                reservationless: this.props.filter,
                export:1
            }
        };
        axios.get(CONSTANT.API_PATH + "/getAllClients", data)
            .then((response) => {
                console.log(response.data);
                this.setState({ loading: false });
                this.handleModal();

                var xlsColumns = ["ID", "User Name", "Password", "Name", "Last Name", "Email", "Phone","Status"];

                var xlsData = [];
                for (var res of response.data.data) {
                    console.log(res)
                   
                    var xlsRow = [
                        res.USER_ID,
                        res.USER_NAME,
                        res.PASS,
                        res.NAME,
                        res.LAST_NAME,
                        res.EMAIL,
                        res.PHONE,
                        res.STATUS == 1 ? 'Active' : 'Disable',
                        
                    ]
       

                    xlsData.push(xlsRow);
                }

                var multiDataSet = [
                    {
                        columns: xlsColumns,
                        data: xlsData
                    },
                ];
                this.setState({ multiDataSet: multiDataSet })
                console.log(multiDataSet)
            })
            .catch((error) => {
                console.log(error);
            })

    }
    render() {

        return (
            <div >
                {this.state.loading ? (
                    <LoadingScreen />
                ) : null}
                <Button variant="secondary" className={this.props.className} onClick={this.generateDataSet}>
                    {this.props.icon}  {this.props.btnTxt}
                </Button>


                <Modal show={this.state.show} onHide={this.handleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title><i class="far fa-file-excel fa-2x"></i></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Excel file was successfully generated!</Modal.Body>
                    <Modal.Footer>
                        <ExcelFile filename={"Full Report-" + new Date().toJSON().slice(0, 10).replace(/-/g, '/')} element={<button className='btn btn-success'><i class="fas fa-file-download"></i> Download</button>}>
                            <ExcelSheet dataSet={this.state.multiDataSet} name="Organization" />
                        </ExcelFile>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default ButtonExport;