import React from 'react';
import { Row, Col, Card, Form, Button, Modal } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import ButtonExport from './Components/ButtonExport';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import Select from '../Reports/Components/Select';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Clients extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            pages: 0,
            user: {},
            term: "",
            filter: 0,
            deleteId: -1,
            showModal: false
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.searchData(0);

    }
    searchData = (page) => {
        console.log("Search");
        this.setState({ loading: true, currentPage: page })
        const { filter } = this.state;
        var postData = {
            params: {
                page: page,
                txt: this.state.term,
                reservationless: filter
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllClients", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loading: false,
                    data: response.data.data,
                    pages: response.data.pages

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };

    handleFilter = val => {
        console.log(val);
        this.setState({
            filter: val
        })
    }


    handleDelete = val => {
        console.log(val)
        console.log("res", val)
        this.setState({
            deleteId: val
        }, () => {
            this.handleModal();
        })
    }

    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }

    confirmDelete = () => {
        const { t } = this.props;
        const { deleteId } = this.state;

        var params = {
            user_id: deleteId,
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/deleteClient", params)
            .then((response) => {
                if (response.data.error == 0) {
                    this.handleModal();
                    this.searchData(0);

                    toast.success(t('Client #') + deleteId + ' ' + t("was successfully removed!"), {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });
    }


    render() {
        const { t } = this.props;

        return (
            <Aux>
                <ToastContainer
                    hideProgressBar
                    newestOnTop={false}
                />
                <Modal show={this.state.showModal} onHide={this.handleModal} >
                    <Modal.Header closeButton style={{ background: "#f44236", color: "#fff" }}>
                        <Modal.Title>{t("Are you sure to delete user #")}{this.state.deleteId}?</Modal.Title>
                    </Modal.Header>

                    <Modal.Footer>
                        <Button variant="danger" size="sm" onClick={this.confirmDelete}>
                            {t("Confirm")}
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Row>
                    <Col>
                        <Card>

                            <Card.Body>
                                <Row>

                                    <Col xs={8}>
                                        <Form.Control
                                            type="text"
                                            placeholder="Search"
                                            style={{ marginTop: 18 }}
                                            disabled={this.state.searchBy == 0 ? true : false}
                                            onChange={(e) => { this.setState({ term: e.target.value }) }}
                                        />
                                    </Col>
                                    <Col >
                                        <Button variant="info" style={{ marginTop: 18 }} onClick={() => this.searchData(0)}>
                                            <span className="fas fa-search"></span>
                                        </Button>
                                    </Col>

                                </Row>
                                <Row>
                                    <Col xs={3}>
                                        <Select label={t("Filter")} options={[
                                            {
                                                name: t('ALL'),
                                                value: 0
                                            },
                                            {
                                                name: t('NO RESERVATIONS'),
                                                value: 1
                                            },
                                        ]}
                                            onChange={this.handleFilter} />
                                    </Col>
                                </Row>


                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Clients")}</Card.Title>
                                <Row style={{ marginTop: 10 }}>
                                    <Col>
                                        <Link
                                            className="btn btn-success btn-sm"
                                            to={{
                                                pathname: "/proyectos/registrar",

                                            }}
                                        > {t("Add")}  <i className="fas fa-plus"></i></Link>
                                    </Col>
                                </Row>
                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (this.state.data.length > 0 ? (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <ReactHTMLTableToExcel
                                                id="test-table-xls-button"
                                                className="btn btn-sm btn-info"
                                                table="table-to-xls"
                                                filename={"Clients report page_" + (1 + this.state.currentPage) + "_" + new Date().toJSON().slice(0, 10).replace(/-/g, '/')}
                                                sheet="tablexls"
                                                buttonText={t('Export actual page')} />

                                            <ButtonExport
                                                btnTxt={t('Export all table content')}
                                                className="btn btn-sm btn-info"
                                                term={this.state.term}
                                                filter={this.state.filter}
                                            />
                                        </div>





                                        <TableComponent data={this.state.data} adds={this.state.adds} handleDelete={this.handleDelete} />

                                        <ReactPaginate
                                            previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                            nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}

                                            forcePage={this.state.currentPage}
                                            pageCount={this.state.pages}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={'pagination'}
                                            subContainerClassName={'pages pagination'}
                                            activeClassName={'active'}
                                        />
                                    </div>) : (
                                        <div className='noResults'>
                                            <span>{t("No results")}</span>
                                        </div>
                                    )
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(Clients);