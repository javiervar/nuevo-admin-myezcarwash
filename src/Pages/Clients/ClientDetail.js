import React from 'react';
import { Row, Col, Card, Modal, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from '../Reservations/Components/TableComponent';

import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import '../Reservations/Reports.css'
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';

class ClientDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            loading: true,
            loadingHistory: true,
            loadingRetailers: false,
            loadingOtherRet: true,
            loadingAssign: false,
            retailers: [],
            info: [],
            pages: 0,
            currentPage: 1,
            searchBy: 0,
            searviceType: 1,
            dateFrom: 0,
            dateTo: 0,
            term: 0,
            status: -1,
            historyData: [],
            historyAdds: [],
            othersRetailers: [],
            showModal: false,
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.getInfo();
        this.getHistoryReservations(0);
        this.getEmployeeRetailers();
        this.getNotRetailers();

    }
    getInfo = () => {
        this.setState({ loading: true })
        var postData = {
            params: {
                user_id: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getUserGeneralInfo", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loading: false,
                    info: response.data.data[0],

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getNotRetailers = () => {
        this.setState({ loadingOtherRet: true })
        var postData = {
            params: {
                employee: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getNotRetailers", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingOtherRet: false,
                    othersRetailers: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getEmployeeRetailers = () => {
        this.setState({ loadingRetailers: true })
        var postData = {
            params: {
                employee: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getEmployeeRetailers", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingRetailers: false,
                    retailers: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getHistoryReservations = (page) => {
        this.setState({ loadingHistory: true, currentPage: page });
        const { searviceType, searchBy, dateFrom, dateTo, term, status } = this.state;
        var option = 1;
        var dateToTemp = dateTo;
        if (dateFrom != 0) {
            option = 2;
            if (dateToTemp == 0) {
                dateToTemp = new Date().toJSON().slice(0, 10);
            }
        }
        var postData = {
            params: {
                employeeId: this.props.location.state[0].id,
                opc: option,
                from: dateFrom,
                to: dateToTemp,
                service: searviceType,
                page: page,
                by: searchBy,
                txt: term,
                status: 3,
                retailer: -1
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllReservations", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loadingHistory: false,
                    historyData: response.data.data,
                    historyAdds: response.data.adds,
                    pages: response.data.pages
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };


    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }

    assign = (id) => {
        console.log(id)
        var params = {
            employee: this.props.location.state[0].id,
            retailer: id
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/assignEmployee", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("This employee was assign successfully to retailer #" + id);
                    this.setState({
                        loadingAssign: false,
                    }, () => {
                        this.handleModal();
                        this.getEmployeeRetailers();
                        this.getNotRetailers();
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loadingAssign: false,
                    });
                }
            });
    }

    revoke = (id) => {
        console.log(id)
        var params = {
            employee: this.props.location.state[0].id,
            retailer: id
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/revokeEmployee", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("This employee was revoke successfully to retailer #" + id);
                    this.setState({
                        loadingAssign: false,
                    }, () => {
                        this.handleModal();
                        this.getEmployeeRetailers();
                        this.getNotRetailers();
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loadingAssign: false,
                    });
                }
            });
    }

    render() {
        const { info } = this.state;
        const { t } = this.props;
        return (
            <Aux>
                
                <Row>
                    <Col>
                        {this.state.loading ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("User")} # {info.USER_ID}</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Row style={{ paddingBottom: 5, borderBottom: "1px solid #ddd" }}>
                                            <Col md={12}>
                                                <h5>{info.US_TYPE}</h5>
                                            </Col>
                                            <Col md={6}>
                                                <p>
                                                    <b>{t("User Name")}: </b>{info.USER_NAME}
                                                </p>
                                                <p>
                                                    <b>{t("Email")}: </b>
                                                    <a href={"mailto:"+info.EMAIL}>
                                                        {info.EMAIL}
                                                    </a>
                                                </p>
                                                <p>
                                                    <b>{t("Password")}: </b>{info.PASS}
                                                </p>
                                                <p>
                                                    <b>{t("Name")}: </b>{info.NAME} {info.LAST_NAME}
                                                </p>
                                                <p>
                                                    <b>{t("Phone")}: </b>{info.PHONE}
                                                </p>

                                            </Col>

                                        </Row>

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                   
                </Row>
                <Row>
                    <Col>
                        {this.state.loadingHistory ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("Booking history")}</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        {this.state.historyData.length > 0 ? (
                                            <div>
                                                <TableComponent data={this.state.historyData} adds={this.state.historyAdds} />

                                                <ReactPaginate
                                                    previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                                    nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}

                                                    forcePage={this.state.currentPage}
                                                    pageCount={this.state.pages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={5}
                                                    onPageChange={this.handlePageClick}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </div>
                                        ) : (
                                                <div className='noResults'>
                                                    <span>{t("No results")}</span>
                                                </div>
                                            )}

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(ClientDetail);