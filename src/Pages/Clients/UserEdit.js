import React from 'react';
import { Row, Col, Card, Form, Button,Select } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from '../Reservations/Components/TableComponent';

import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import '../Reservations/Reports.css'
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class UserEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            loading: true,
            name:"",
            lastName:"",
            pass:"",
            userNAME:"",
            email:"",
            phone:""

        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.getInfo();
    }
    getInfo = () => {
        this.setState({ loading: true })
        var postData = {
            params: {
                user_id: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getUserGeneralInfo", postData)
            .then((response) => {
                console.log(response);
                var user=response.data.data[0];
                this.setState({
                    loading: false,
                    info: response.data.data[0],
                    name:user.NAME,
                    lastName:user.LAST_NAME,
                    pass:user.PASS,
                    userNAME:user.USER_NAME,
                    email:user.EMAIL,
                    phone:user.PHONE,
                    person_id:user.PERSON_ID

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        const { name, lastName, pass, phone,person_id } = this.state;
        var params = {
            user_id: this.props.location.state[0].id,
            name: name,
            last_name:lastName,
            pass:pass,
            phone:phone,
            person_id:person_id
        }
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/editUser", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("User was updated successfully");
                    this.setState({
                        loading: false,
                    })
                } else {
                    alert("ERROR:",response.data.text)
                    this.setState({
                        loading: false,
                    });
                }
            });
    }


    render() {
        const { info } = this.state;
        const{t}=this.props;
        return (
            <Aux>

                <Row>
                    <Col>
                        {this.state.loading ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">{t("User")} # {info.USER_ID}</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Form onSubmit={this.onFormSubmit}>

                                            <Form.Group size="sm">
                                                <Form.Label>{t("Password")}: </Form.Label>
                                                <Form.Control type="text" name="password"
                                                    value={this.state.pass}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                pass: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group size="sm">
                                                <Form.Label>{t("Name")}: </Form.Label>
                                                <Form.Control type="text" name="name"
                                                    value={this.state.name}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                name: e.target.value
                                                            })
                                                    }}
                                                    minLength={2}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group size="sm">
                                                <Form.Label>{t("Last Name")}: </Form.Label>
                                                <Form.Control type="text" name="lastName"
                                                    value={this.state.lastName}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                lastName: e.target.value
                                                            })
                                                    }}
                                                    minLength={2}
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group size="sm">
                                                <Form.Label>{t("Phone")}: </Form.Label>
                                                <Form.Control type="text" name="phone"
                                                    value={this.state.phone}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                phone: e.target.value
                                                            })
                                                    }}
                                                    minLength={1}
                                                    required
                                                />
                                            </Form.Group>

                                            <Row className="controller">
                                                <Col>
                                                    <Button type="submit" style={{ float: 'right' }}>{t("Update")}</Button>
                                                </Col>
                                            </Row>
                                        </Form>

                                    </Card.Body>
                                </Card>
                            )}
                    </Col>

                </Row>

            </Aux>
        );
    }
}

export default withNamespaces()(UserEdit);