import React from 'react';
import {Spinner} from 'react-bootstrap';
import './Loading.css';   


const LoadingScreen = (props) => {
    return (
        <div style={{textAlign:'center'}}>
            <Spinner animation="grow" variant="info">
                <span className="sr-only">{props.message}</span>
            </Spinner>
        </div>
    )
}

LoadingScreen.defaultProps = {
    message: 'Loaging...'
}

export default LoadingScreen;