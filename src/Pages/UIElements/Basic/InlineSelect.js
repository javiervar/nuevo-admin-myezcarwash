import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import './selectstyle.css'


class InlineSelect extends React.Component {
    state = { value: 1, default: 0 };
    loadOptions = () => {
        const options = this.props.options.map((opc, index) => {
            return <option key={index} value={opc.value}>{opc.name}</option>;
        })


        return options;
    }

    handleChange = (val) => {
        console.log(val.target.value)
        this.props.onChange(val.target.value)
    }

    render() {

        return (

            <Form.Group as={Row} controlId="formPlaintextEmail">
                <Form.Label column sm="3">
                    {this.props.label}
                </Form.Label>
                <Col sm="9">
                    <Form.Control as="select" defaultValue={this.props.defaultValue} required={this.props.required} onChange={this.handleChange} className="SelectStyle">
                        {this.loadOptions()}
                    </Form.Control>
                </Col>
            </Form.Group>
        );
    }

}
InlineSelect.defaultProps = {
    required: false
}
export default InlineSelect;