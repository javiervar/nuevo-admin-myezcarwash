import React from 'react';
import { Spinner } from 'react-bootstrap';
import './Loading.css';


const Loading = (props) => {
    return (
        <div className="loadingScreen">
            <div className="loading">
                <Spinner animation="grow" variant="info">
                </Spinner>
                <br></br>
                <span className="textLoading">{props.message}</span>
            </div>

        </div>
    )
}

Loading.defaultProps = {
    message: 'Loading data...'
}

export default Loading;