import React from 'react';
import axios from 'axios';
import { Button, Modal } from 'react-bootstrap';
import LoadingScreen from '../../UIElements/Basic/LoadingScreen';
import ReactExport from 'react-data-export';
import CONSTANT from '../../../store/constant';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

class ButtonExport extends React.Component {
    state = { show: false, loading: false, multiDataSet: [] };
    handleModal = () => {
        this.setState({ show: !this.state.show });
    }
    generateDataSet = () => {
        this.setState({ loading: true });
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRlIjoiMjAxOVwvMDZcLzEyIiwidHlwZSI6IjMiLCJpZCI6IjMyIiwiZW1haWwiOiJlZGdhci52YWxkZXpyQGdtYWlsLmNvbSJ9.7R_ekvoTF2TtwFhz4V7qKOOxLLZHxigLw1FiCNPRJ3s';
        var data = this.props.postData;
        data.params.export = 1;
        axios.get(CONSTANT.API_PATH + "/getAllReservations", data)
            .then((response) => {
                console.log(response.data);
                this.setState({ loading: false });
                this.handleModal();

                var xlsColumns = ["ID", "CATEGORY", "STATUS", "DATE", "HOUR", "RETAILER NAME", "LOCATION SERVICE", "SERVICE", "# EMPLOYEE", "FIRST NAME", "LAST NAME", "CUSTOMER FIRST NAME", "CUSTOMER LAST NAME", "PHONE", "E-MAIL", "MAKE", "MODEL", "YEAR", "COLOR", "PLATE", "PRICE"];

                for (var adds of response.data.adds) {
                    xlsColumns.push(adds.ADDITIONAL_NAME);
                }
                xlsColumns.push("TOTAL ADDS");
                xlsColumns.push("TAX %");
                xlsColumns.push("TAX AMOUNT");
                xlsColumns.push("PROMO CODE");
                xlsColumns.push("PROMO DISCOUNT");
                xlsColumns.push("PROMO AMOUNT");
                xlsColumns.push("TOTAL PAID");
                xlsColumns.push("TRANSACTION FEE (3%) $");
                xlsColumns.push("TIP PAID");


                var xlsData = [];
                for (var res of response.data.data) {
                    console.log(res)
                    let promoAmount = 0;
                    if (res.PROMO_DISCOUNT !== null) {
                        promoAmount = (res.TOTAL * (res.PROMO_DISCOUNT / 100)) / (1 - (res.PROMO_DISCOUNT / 100));
                        promoAmount = promoAmount.toFixed(3);
                    }
                    var xlsRow = [
                        res.RESERVATION_ID,
                        res.SERVICE_TYPE,
                        res.STATUS,
                        res.DATE,
                        res.HOUR,
                        res.RETAILER,
                        res.LOCATION,
                        res.SERVICE_NAME,
                        res.EMPLOYEE,
                        res.EMPLOYEE_NAME,
                        res.EMPLOYEE_LASTNAME,
                        res.CUSTOMER_NAME,
                        res.CUSTOMER_LASTNAME,
                        res.PHONE,
                        res.EMAIL,
                        res.MAKE_NAME,
                        res.MODEL_NAME,
                        res.YEAR,
                        res.COLOR,
                        res.PLATE,
                        parseFloat(res.PRICE)
                    ]
                    for (var add of res.ADDITIONALS) {
                        if (add.CHECK) {
                            xlsRow.push(parseFloat(add.PRICE))
                        } else {
                            xlsRow.push(0)
                        }
                    }
                    xlsRow.push(parseFloat(res.TOTAL_ADDITIONAL));
                    xlsRow.push(res.TAX);
                    xlsRow.push(parseFloat(parseFloat((parseFloat(res.TOTAL_ADDITIONAL) + parseFloat(res.PRICE)) * (res.TAX / 100)).toFixed(2)));
                    xlsRow.push(res.CODE);
                    xlsRow.push(parseFloat(res.PROMO_DISCOUNT));
                    xlsRow.push(parseFloat(promoAmount));
                    xlsRow.push(parseFloat(res.TOTAL));
                    xlsRow.push(parseFloat(parseFloat(res.TOTAL-(res.TOTAL/1.03)).toFixed(2)));
                    xlsRow.push(parseFloat((res.TIP==""||res.TIP==null)?0:res.TIP));







                    xlsData.push(xlsRow);
                }

                var multiDataSet = [
                    {
                        columns: xlsColumns,
                        data: xlsData
                    },
                ];
                this.setState({ multiDataSet: multiDataSet })
                console.log(multiDataSet)
            })
            .catch((error) => {
                console.log(error);
            })

    }
    render() {

        return (
            <div >
                {this.state.loading ? (
                    <LoadingScreen />
                ) : null}
                <Button variant="secondary" className={this.props.className} onClick={this.generateDataSet}>
                    {this.props.icon}  {this.props.btnTxt}
                </Button>


                <Modal show={this.state.show} onHide={this.handleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title><i class="far fa-file-excel fa-2x"></i></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Excel file was successfully generated!</Modal.Body>
                    <Modal.Footer>
                        <ExcelFile filename={"Full Report-" + new Date().toJSON().slice(0, 10).replace(/-/g, '/')} element={<button className='btn btn-success'><i class="fas fa-file-download"></i> Download</button>}>
                            <ExcelSheet dataSet={this.state.multiDataSet} name="Organization" />
                        </ExcelFile>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default ButtonExport;