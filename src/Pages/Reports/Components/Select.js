import React from 'react';
import { Form } from 'react-bootstrap';
import './selectstyle.css'


class Select extends React.Component {
    state = {value:1,default:0};
    loadOptions = () => {
        const options = this.props.options.map((opc,index) => {
            return <option key={index} value={opc.value}>{opc.name}</option>;
        })

       
        return options;
    }

    handleChange=(val)=>{
        console.log(val.target.value)
        this.props.onChange(val.target.value)
    }

    render() {

        return (
            <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>{this.props.label}</Form.Label>
                <Form.Control  as="select"  defaultValue={this.props.defaultValue} required={this.props.required} onChange={this.handleChange} className="SelectStyle">
                    {this.loadOptions()}
                </Form.Control>
            </Form.Group>
        );
    }

}
Select.defaultProps={
    required:false
}
export default Select;