import React from 'react';
import { Table } from 'react-bootstrap';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableReport extends React.Component {
    state = {adds:null}
    renderAddsName=()=>{
        const adds=this.props.adds.map((add,index)=>{
            return(
                <th key={add.ADDITIONAL_ID}>{add.ADDITIONAL_NAME}</th>
            );
        })

        return adds;
    };
    renderRows = () => {
        const rows = this.props.data.map((row, index) => {
            let promoAmount=0;
            if(row.PROMO_DISCOUNT!==null){
                promoAmount=(row.TOTAL*(row.PROMO_DISCOUNT/100))/(1-(row.PROMO_DISCOUNT/100));
                promoAmount=promoAmount.toFixed(3);
            }

            var status = "";
            switch (row.STATUS) {
                case '1':
                    status = "PAID";
                    break;
                case '2':
                    status = "TAKEN";
                    break;
                case '3':
                    status = "DONE";
                    break;
                case '4':
                    status = "CANCELED";
                    break;
                case '5':
                    status = "PAID RET";
                    break;
            }
            
            return (
                <tr key={index}>
                    <th scope="row">{row.RESERVATION_ID}</th>
                    <td>{row.SERVICE_TYPE==='1'?'Stationary':'Mobile'}</td>
                    <td>{status}</td>
                    <td>{row.DATE}</td>
                    <td>{row.HOUR}</td>
                    <td>{row.RETAILER}</td>
                    <td>{row.LOCATION}</td>
                    <td>{row.SERVICE_NAME}</td>  
                    <td>{row.EMPLOYEE}</td>
                    <td>{row.EMPLOYEE_NAME}</td>
                    <td>{row.EMPLOYEE_LASTNAME}</td>
                    <td>{row.CUSTOMER_NAME}</td>
                    <td>{row.CUSTOMER_LASTNAME}</td>
                    <td>{row.PHONE}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.MAKE_NAME}</td>
                    <td>{row.MODEL_NAME}</td>
                    <td>{row.YEAR}</td>
                    <td>{row.COLOR}</td>
                    <td>{row.PLATE}</td>
                    <td>{row.PRICE}</td>
                    {row.ADDITIONALS.map((head,index)=>{
                        return <td key={index}>{head.CHECK?head.PRICE:0}</td>
                    })}
                    <td>{row.TOTAL_ADDITIONAL}</td>
                    <td>{row.TAX}</td>
                    <td>{((parseFloat(row.TOTAL_ADDITIONAL)+parseFloat(row.PRICE))*(row.TAX/100)).toFixed(2)}</td>
                    <td>{row.CODE}</td>
                    <td>{row.PROMO_DISCOUNT} %</td>
                    <td>{promoAmount}</td>
                    <td>{row.STATUS==4?"-":""}{row.TOTAL}</td>
                    <td>{(row.TOTAL-(row.TOTAL/1.03)).toFixed(2)}</td>
                    <td>{row.TIP}</td>
                    
                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls"> 

                <thead key="thead">

                    <tr>
                        <th colSpan="8" rowSpan="2">{t("SERVICE INFORMATION")}</th>
                        <th colSpan="3" rowSpan="2">{t("EMPLOYEE INFORMATION")}</th>
                        <th colSpan="4" rowSpan="2">{t("CUSTOMER INFORMATION")}</th>
                        <th colSpan="5" rowSpan="2">{t("VEHICLE INFORMATION")}</th>
                        <th colSpan="30">{t("TRANSACTION INFORMATION")}</th>
                    </tr>
                    <tr>
                        <th colSpan="1">{t("SERVICE")}</th>
                        <th colSpan="17">ADDS</th>
                        <th colSpan="2">{t("TAX")}</th>
                        <th colSpan="10">TOTAL</th>
                    </tr>


                    <tr>
                        <th>ID</th>
                        <th>{t("CATEGORY")}</th>
                        <th>{t("STATUS")}</th>
                        <th>{t("DATE")}</th>
                        <th>{t("HOUR")}</th>
                        <th>{t("RETAILER NAME")}</th>
                        <th>{t("LOCATION SERVICE")}</th>
                        <th>{t("SERVICE")} </th>
                        <th># {t("EMPLOYEE")} </th>
                        <th>{t("FIRST NAME")}</th>
                        <th>{t("LAST NAME")}</th>
                        <th>{t("CUSTOMER FIRST NAME")} </th>
                        <th>{t("CUSTOMER LAST NAME")}</th>
                        <th>{t("PHONE")}</th>
                        <th>{t("E-MAIL")}</th>
                        <th>{t("MAKE")}</th>
                        <th>{t("MODEL")}</th>
                        <th>{t("YEAR")}</th>
                        <th>{t("COLOR")}</th>
                        <th>{t("PLATE")}</th>
                        <th>{t("PRICE")}</th>
                        {this.renderAddsName()}
                       
                        {/**TOTAL */}
                        <th>{t("TOTAL ADDS")}</th>
                        <th>{t("TAX")} %</th>
                        <th>{t("TAX AMOUNT")}</th>
                        <th>{t("PROMO CODE")}</th>
                        <th>{t("PROMO DISCOUNT")} %</th>
                        <th>{t("PROMO AMOUNT")} $</th>
                        <th>{t("TOTAL PAID")} $</th>
                        <th>{t("TRANSACTION FEE")} (3%) $</th>
                        <th>{t("TIP PAID")} $</th>
                    </tr>
                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableReport);