import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import './Reports.css'
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Link } from 'react-router-dom'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import DEMO from "../../store/constant";
import TableReport from './Components/TableReport';
import ButtonExport from './Components/ButtonExport'
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import Select from './Components/Select';
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
const selecteData = {
    opc_status: [
        {
            name: 'ALL',
            value: -1
        },
        {
            name: 'PAID',
            value: 1
        },
        {
            name: 'TAKEN',
            value: 2,
        }, {
            name: 'DONE',
            value: 3
        }

    ],
    opc_service: [
        {
            name: 'STATIONARY',
            value: 1
        },
        {
            name: 'MOBILE',
            value: 2
        }
    ],
    opc_search: [
        {
            name: 'ALL',
            value: 0
        },
        {
            name: 'CLIENT',
            value: 1
        },
        {
            name: 'EMPLOYEE',
            value: 2
        },
        {
            name: 'RETAILER',
            value: 3
        },
        {
            name: 'LOCATION',
            value: 4
        }


    ]
};
class Reports extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pages: 0,
            adds: [],
            data: [],
            currentPage: 1,
            loading: true,
            opc_service: [],
            opc_search: [],
            searchBy: 0,
            searviceType: 1,
            dateFrom: 0,
            dateTo: 0,
            term: 0,
            status: -1,
            retailer: -1,
            employee: -1,
            user: {},
            postData: {}
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        this.searchData(0);

    }
    searchData = (page) => {
        console.log("Search");
        this.setState({ loading: true, currentPage: page })
        const { searviceType, searchBy, dateFrom, dateTo, term, status, retailer, employee } = this.state;
        var option = 1;
        var dateToTemp = dateTo;
        if (dateFrom != 0) {
            option = 2;
            if (dateToTemp == 0) {
                dateToTemp = new Date().toJSON().slice(0, 10);
            }
        }


        var postData = {
            params: {
                opc: option,
                from: dateFrom,
                to: dateToTemp,
                service: searviceType,
                page: page,
                by: searchBy,
                txt: term,
                status: status,
                retailer: retailer,
                employeeId: employee,
                export:-1
            }
        };
        console.log(postData);

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllReservations", postData)
            .then((response) => {
                console.log(response);
                this.setState({ data: response.data.data, pages: response.data.pages, adds: response.data.adds, loading: false, postData: postData })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };

    handleServiceType = val => {
        console.log('servicetype ', val)
        this.setState({ searviceType: val })
    }
    handleSearchBy = val => {
        console.log('searchby ', val)
        this.setState({ searchBy: val })
    }

    handleStatus = val => {
        console.log('status ', val)
        this.setState({ status: val })
    }

    handleDateFrom = val => {
        console.log('datefrom ', val)
        this.setState({ dateFrom: val.target.value })
    }

    handleDateTo = val => {
        console.log('dateto ', val)
        this.setState({ dateTo: val.target.value })
    }


    render() {
        const { t } = this.props;
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>

                            <Card.Body>
                                <Row>
                                    <Col xs={12} md={2}>
                                        <Select label={t("Search by")} options={selecteData.opc_search} onChange={this.handleSearchBy} />
                                    </Col>
                                    <Col xs={12} md={8}>
                                        <Form.Control
                                            type="text"
                                            placeholder={t("Search")}
                                            style={{ marginTop: 18 }}
                                            disabled={this.state.searchBy == 0 ? true : false}
                                            onChange={(e) => { this.setState({ term: e.target.value }) }}
                                        />
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <Button variant="info" style={{ marginTop: 18 }} onClick={() => this.searchData(0)}>
                                            <span className="fas fa-search"></span>
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>

                                    <Col xs={6} md={3}>
                                        <Select label={t("Service type")} options={selecteData.opc_service} onChange={this.handleServiceType} />
                                    </Col>

                                    <Col xs={6} md={3}>
                                        <Select label={t("Status")} options={selecteData.opc_status} onChange={this.handleStatus} />
                                    </Col>

                                    <Col xs={6} md={3}>
                                        <Form.Group>
                                            <Form.Label>{t("From")}</Form.Label>
                                            <Form.Control size="sm" type="date" onChange={this.handleDateFrom} />
                                        </Form.Group>
                                    </Col>
                                    <Col xs={6} md={3}>
                                        <Form.Group>
                                            <Form.Label>{t("To")}</Form.Label>
                                            <Form.Control size="sm" type="date" onChange={this.handleDateTo} />
                                        </Form.Group>
                                    </Col>


                                </Row>


                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Report")}</Card.Title>
                                <Link
                                    className="btn btn-success btn-sm"
                                    to={{
                                        pathname: "/reservations/pay",
                                    }}
                                >{t("Pay to retailer")}</Link>
                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (this.state.data.length > 0 ? (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <ReactHTMLTableToExcel
                                                id="test-table-xls-button"
                                                className="btn btn-sm btn-info"
                                                table="table-to-xls"
                                                filename={"Report page_" + (1 + this.state.currentPage) + "_" + new Date().toJSON().slice(0, 10).replace(/-/g, '/')}
                                                sheet="tablexls"
                                                buttonText={t('Export actual page')} />


                                            <ButtonExport btnTxt={t('Export all table content')} className="btn btn-sm btn-info" postData={this.state.postData} />
                                        </div>
                                        <TableReport data={this.state.data} adds={this.state.adds} />

                                        <ReactPaginate
                                            previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                            nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}

                                            forcePage={this.state.currentPage}
                                            pageCount={this.state.pages}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={'pagination'}
                                            subContainerClassName={'pages pagination'}
                                            activeClassName={'active'}
                                        />
                                    </div>) : (
                                        <div className='noResults'>
                                            <span>{t("No results")}</span>
                                        </div>
                                    )
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(Reports);