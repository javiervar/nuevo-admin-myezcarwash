import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const{t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{row.SERVICE_ID}</th>
                    <td>{row.SERVICE_NAME}</td>
                    <td >{JSON.parse(row.DESCRIPTION).map(val=>{
                        return val;
                    })}</td>
                    <td>{row.SERVICE_TYPE==1?'Stationary':'Mobile'}</td>
                    <td>{row.PRICE}</td>
                    <td>{row.ORDER_ID}</td>
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/services/service",
                                state: [{ company: row.SERVICE_ID }]
                            }}
                        >{t("Show")}</Link>
                    </td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>Id</th>
                        <th>{t("Name")}</th>
                        <th>{t("Description")}</th>
                        <th>{t("Service type")}</th>
                        <th>{t("Price")}</th>
                        <th>{t("Order in app")}</th>
                        <th>{t("Command")}</th>
                        
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);