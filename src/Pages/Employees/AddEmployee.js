import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import Select from '../Reports/Components/Select';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class AddEmployee extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            user: {},
            userName: "",
            userType: 4,
            firstName: "",
            lastName: "",
            pass: "",
            directPhone: "",
            email: "",
            retailer: -1,
            retailerItems: []

        }
    }

    componentWillMount() {

        if (this.props.location.state !== undefined) {
            this.setState({
                retailer: this.props.location.state[0].id
            })
        }
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user,
            }, () => {
                this.getRetailersItems();
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
    }


    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });

        var params = this.state
        console.log(params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addEmployee", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("New employee was added successfully");
                    this.setState({
                        loading: false,
                        userName: "",
                        userType: 2,
                        firstName: "",
                        lastName: "",
                        pass: "",
                        directPhone: "",
                        email: "",
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loading: false,
                    });
                }
            });
    }

    getRetailersItems = () => {

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getRetailerItems")
            .then((response) => {
                console.log(response);
                response.data.data.unshift({ name: 'All', value: -1 })
                this.setState({
                    retailerItems: response.data.data,
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }

    handleRetailer = val => {
        console.log('retailer ', val)
        this.setState({
            retailer: val
        })
    }



    render() {
        const{t}=this.props;
        return (
            <Aux>
                <Row>
                    <Col>


                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Add")} {t("employee")}
                                
                                
                                </Card.Title>

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Form onSubmit={this.onFormSubmit}>

                                            <Row>

                                                <Col xs={12} md={6}>
                                                    {this.state.retailer == -1 ? (
                                                        <Select label={t("Retailer")} options={this.state.retailerItems} onChange={this.handleRetailer} />

                                                    ) : (
                                                        <p><b>{t("Retailer")}:</b><h3>#{this.state.retailer}</h3></p>
                                                    )}
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Label>{t("User Name")}</Form.Label>
                                                        <Form.Control type="Text" name="userName"
                                                            value={this.state.userName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        userName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                            minLength={4}
                                                        />

                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Password")} </Form.Label>
                                                        <Form.Control type="text" name="pass"
                                                            value={this.state.pass}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        pass: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                            minLength={7}
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("First Name")}:</Form.Label>
                                                        <Form.Control type="text" name="firstName"
                                                            value={this.state.firstName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        firstName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Last Name")}:</Form.Label>
                                                        <Form.Control type="text" name="lastName"
                                                            value={this.state.lastName}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        lastName: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>



                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Direct phone")}:</Form.Label>
                                                        <Form.Control type="text" name="directPhone"
                                                            value={this.state.directPhone}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        directPhone: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                            minLength={10}
                                                        />
                                                    </Form.Group>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>{t("Email")}:</Form.Label>
                                                        <Form.Control type="email" name="email"
                                                            value={this.state.email}
                                                            onChange={(e) => {
                                                                this.setState(
                                                                    {
                                                                        email: e.target.value
                                                                    })
                                                            }}
                                                            required
                                                        />
                                                    </Form.Group>
                                                </Col>

                                            </Row>

                                            <Button variant="primary" type="submit">
                                                {t("Submit")}
                                            </Button>
                                        </Form>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(AddEmployee);