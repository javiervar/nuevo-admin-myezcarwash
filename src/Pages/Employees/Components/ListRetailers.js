import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class ListRetailers extends React.Component {
    state = {}

    renderRows = () => {
        const{t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>

                    <td>
                        <Link
                            to={{
                                pathname: "/retailers/detail",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{row.USER_NAME} {row.USER_ID}</Link>

                        </td>
                    <td>
                        <Button variant="primary" size={"sm"} onClick={()=>this.props.handleOnClick(row.USER_ID)}>{t("Assign")}</Button>
                    </td>


                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>{t("Retailer")}</th>
                        <th>{t("Command")}</th>
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(ListRetailers);