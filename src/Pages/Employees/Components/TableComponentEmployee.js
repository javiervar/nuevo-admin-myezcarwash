import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{row.USER_ID}</th>
                    <td>{row.USER_NAME}</td>
                    <td>{row.PASS}</td>
                    <td>{row.NAME}</td>
                    <td>{row.LAST_NAME}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.PHONE}</td>
                    <td>
                    <Link
                            className="btn btn-link"
                            to={{
                                pathname: "/employees/detail",
                                state: [{ company: row.USER_ID }]
                            }}
                        >{row.RETAILER}</Link>
                    </td>
                    <td>{row.STATUS==1?'Active':'Disable'}</td>
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/employees/detail",
                                state: [{ id: row.USER_ID }]
                            }}
                        >Detail</Link>
                    </td>

                </tr>
            );
        })

        return rows


    }
    render() {
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>Id</th>
                        <th>User name</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Retailer</th>
                        <th>Status</th>
                        <th>Command</th>
                        
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);