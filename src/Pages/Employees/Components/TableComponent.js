import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const { t } = this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/employees/detail",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{t("Details")}</Link>
                        <Link
                            className="btn btn-warning btn-sm"
                            to={{
                                pathname: "/user/edit",
                                state: [{ id: row.USER_ID }]
                            }}
                        >{t("Edit")}</Link>
                    </td>
                    <th scope="row">{row.USER_ID}</th>
                    <td>{row.USER_NAME}</td>
                    <td>{row.PASS}</td>
                    <td>{row.NAME}</td>
                    <td>{row.LAST_NAME}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.PHONE}</td>
                    {this.props.userType == "1" ? (
                        <td>
                            {row.RETAILERS.map((ret, index) => {
                                return (

                                    <Link
                                        key={index}
                                        className="btn btn-link"
                                        to={{
                                            pathname: "/employees/detail",
                                            state: [{ id: ret.USER_ID }]
                                        }}
                                    >{ret.USER_NAME}</Link>
                                )
                            })}

                        </td>
                    ) : null}
                    <td>{row.STATUS == 1 ? 'Active' : 'Disable'}</td>


                </tr>
            );
        })

        return rows


    }
    render() {
        const { t } = this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>{t("Command")}</th>
                        <th>Id</th>
                        <th>{t("User Name")}</th>

                        <th>{t("Password")}</th>
                                                <th>{t("Name")}</th>
                        <th>{t("Last name")}</th>
                        <th>{t("Email")}</th>
                        <th>{t("Phone")}</th>
                        {this.props.userType == "1" ? (
                            <th>{t("Retailer")}</th>
                        ) : null}
                        <th>{t("Status")}</th>


                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);