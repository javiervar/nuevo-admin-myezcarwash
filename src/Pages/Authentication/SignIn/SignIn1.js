import React from 'react';
import { NavLink } from 'react-router-dom';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import { withRouter } from "react-router-dom";
import LoadingScreen from "../../UIElements/Basic/LoadingScreen"
import CONSTANT from '../../../store/constant';
import axios from 'axios';


class SignUp1 extends React.Component {
    state = {
        loading: false,
        user: "",
        pass: "",
    }
    componentWillMount() {

        console.log(localStorage.getItem("adminwash"));
        localStorage.removeItem("adminwash");
        if (localStorage.getItem("adminwash") !== null) {
            this.props.history.push("/home");
        }

    }
    onFormSubmit = (event) => {

        event.preventDefault();
        this.setState({ loading: true });
        const { user, pass } = this.state;
        var params = {
            userName: user,
            pass: pass
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';

        axios.post(CONSTANT.API_PATH + "/login", params)
            .then((response) => {

                var data = response.data;
                if (data.error) {
                    alert(data.message);
                    this.setState({ loading: false });
                } else {
                    localStorage.setItem("adminwash", JSON.stringify(response.data));
                    
                    setTimeout( ()=> {

                        
                        this.props.history.push("/home",);
                        

                    }, 4000);

                }
            });
    }
    render() {
        return (
            <Aux>
                {this.state.loading ? (
                    <LoadingScreen message="Logging in ..." />
                ) : (
                        <div className="auth-wrapper">
                            <div className="auth-content">

                                <div className="card">
                                    <form onSubmit={this.onFormSubmit} className="card-body text-center">
                                        <div className="mb-4">
                                            <i className="feather icon-unlock auth-icon" />
                                        </div>
                                        <h3 className="mb-4">Login</h3>
                                        <div className="input-group mb-3">
                                            <input type="text" className="form-control" placeholder="User/Email"
                                                value={this.state.user}
                                                onChange={(e) => {
                                                    this.setState({
                                                        user: e.target.value
                                                    })
                                                }}
                                                required
                                            />
                                        </div>
                                        <div className="input-group mb-4">
                                            <input type="password" className="form-control" placeholder="password"
                                                value={this.state.pass}
                                                onChange={(e) => {
                                                    this.setState(
                                                        {
                                                            pass: e.target.value
                                                        })
                                                }}
                                                required
                                            />
                                        </div>

                                        <button className="btn btn-primary shadow-2 mb-4" type="submit">Login</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    )}
            </Aux>
        );
    }
}

export default SignUp1;