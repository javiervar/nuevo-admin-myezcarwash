import React from 'react';
import { Row, Col, Card, Tabs, Tab, Modal, Button } from 'react-bootstrap';
import axios from 'axios';

import Aux from "../../hoc/_Aux";
import CONSTANT from "../../store/constant";
import Loading from "../UIElements/Basic/Loading"
import avatar1 from '../../assets/images/user/avatar-1.jpg';
import avatar2 from '../../assets/images/user/avatar-2.jpg';
import avatar3 from '../../assets/images/user/avatar-3.jpg';
import UpcomingList from './Components/UpcomingList';
import ScheduleComponent from './Components/ScheduleComponent';
import ReservationInfo from './Components/ReservationInfo';
import Select from '../Reports/Components/Select';
import SalesComponent from './Components/SalesComponent';
import { Link } from 'react-router-dom'
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
const selecteData = {
    opc_service: [
        {
            name: 'STATIONARY',
            value: 1
        },
        {
            name: 'MOBILE',
            value: 2
        }
    ],
}


class Dashboard extends React.Component {
    state = {
        user: {},
        location: -1,
        scheduleLoading: false,
        salesLoading: false,
        sales: {},
        events: [],
        upcomingReservations: [],
        locationItems: [],
        retailerItems: [],
        showModal: false,
        reservationId: -1,
    }
    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            }, (() => {
                this.getUpcomingReservation(6);

                if (user.type == '1') {
                    this.getRetailersItems();
                } else if (user.type == '2') {
                    this.getLocationItems();
                }
                this.getReservationEvents();
                this.getStats();
            }))
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

   


    getUpcomingReservation = (quantity) => {
        this.setState({ loading: true })
        var postData = {
            params: {
                quantity: quantity,
                retailer: this.state.user.type == '2' ? this.state.user.id : -1,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getUpcomingReservations", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    scheduleLoading: false,
                    upcomingReservations: response.data.data,
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getRetailersItems = () => {

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getRetailerItems")
            .then((response) => {
                console.log(response);
                response.data.data.unshift({ name: 'All', value: -1 })
                this.setState({
                    retailerItems: response.data.data,
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getLocationItems = (retailer) => {
        var postData = {
            params: {
                retailer: this.state.user.type == '2' ? this.state.user.id : retailer,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getLocationItems", postData)
            .then((response) => {
                console.log(response);
                response.data.data.unshift({ name: 'Select a location', value: -1 })
                this.setState({
                    locationItems: response.data.data,
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }
    getReservationInfo = reservation => {
        this.setState({
            reservationId: reservation
        })
        var postData = {
            params: {
                reservation: reservation,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getReservationInfo", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    reservationInfo: response.data,
                    showModal: true,
                });

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getReservationEvents = () => {
        this.setState({ scheduleLoading: true })
        var postData = {
            params: {
                location: this.state.location,
                retailer: this.state.user.type == '2' ? this.state.user.id : -1
            }
        };
        console.log(postData)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getReservationEvents", postData)
            .then((response) => {
                console.log(response);
                const events = response.data.data.map((res) => {
                    var time = res.HOUR.split(":");
                    var date = res.DATE.split('/');
                    var y = parseInt(date[2]);
                    var m = parseInt(date[0]) - 1;
                    var d = parseInt(date[1]);
                    return {
                        id: res.RESERVATION_ID,
                        title: res.LOCATION,
                        allDay: false,
                        start: new Date(y, m, d, time[0], time[1]),
                        end: new Date(y, m, d, parseInt(time[0]) + 1, time[1]),
                        tooltip: res.SERVICE_NAME
                    }
                });
                this.setState({
                    scheduleLoading: false,
                    events: events,
                }, () => {
                    this.getStats();
                })

                console.log("events", events)
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getStats = () => {
        this.setState({ salesLoading: true })

        var postData = {
            params: {
                location: this.state.location,
                retailer: -1,
                employee: -1,
            }
        };
        console.log(postData);

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getAllSales", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    salesLoading: false,
                    sales: response.data.data
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }
    search = () => {
        this.getReservationEvents();
    }

    handleRetailer = val => {
        console.log('retailer ', val)
        this.getLocationItems(val);
    }

    handleLocation = val => {
        console.log('location ', val)
        this.setState({
            location: val
        })
    }

    handleEvent = val => {
        console.log('event ', val);
        this.getReservationInfo(val.id);
    }
    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }
    render() {

        const { t } = this.props;
        const tabContent = (
            <Aux>
                <div className="media friendlist-box align-items-center justify-content-center m-b-20">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar1} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Silje Larsen</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-up f-22 m-r-10 text-c-green" />3784</span>
                    </div>
                </div>
                <div className="media friendlist-box align-items-center justify-content-center m-b-20">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar2} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Julie Vad</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-up f-22 m-r-10 text-c-green" />3544</span>
                    </div>
                </div>
                <div className="media friendlist-box align-items-center justify-content-center m-b-20">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar3} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Storm Hanse</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-down f-22 m-r-10 text-c-red" />2739</span>
                    </div>
                </div>
                <div className="media friendlist-box align-items-center justify-content-center m-b-20">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar1} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Frida Thomse</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-down f-22 m-r-10 text-c-red" />1032</span>
                    </div>
                </div>
                <div className="media friendlist-box align-items-center justify-content-center m-b-20">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar2} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Silje Larsen</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-up f-22 m-r-10 text-c-green" />8750</span>
                    </div>
                </div>
                <div className="media friendlist-box align-items-center justify-content-center">
                    <div className="m-r-10 photo-table">
                        <a href={CONSTANT.BLANK_LINK}><img className="rounded-circle" style={{ width: '40px' }} src={avatar3} alt="activity-user" /></a>
                    </div>
                    <div className="media-body">
                        <h6 className="m-0 d-inline">Storm Hanse</h6>
                        <span className="float-right d-flex  align-items-center"><i className="fa fa-caret-down f-22 m-r-10 text-c-red" />8750</span>
                    </div>
                </div>
            </Aux>
        );

        return (
            <Aux>
                <Modal show={this.state.showModal} onHide={this.handleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Information</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ReservationInfo info={this.state.reservationInfo} />
                    </Modal.Body>
                    <Modal.Footer>

                        <Button variant="secondary" onClick={this.handleModal}>
                            Close
                        </Button>
                        <Link
                            className="btn btn-info"
                            to={{
                                pathname: "/reservations/detail",
                                state: [{ id: this.state.reservationId }]
                            }}
                        >Details</Link>
                    </Modal.Footer>
                </Modal>
                <Row>

                    <Col md={12} xs={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>{t('Schedule')}</Card.Title>
                                <Row>
                                    {this.state.user.type == '1' ? (
                                        <Col>
                                            <Select label={t('Retailer')} options={this.state.retailerItems} onChange={this.handleRetailer} />
                                        </Col>
                                    ) : null}

                                    <Col>
                                        <Select label={t('Location')} options={this.state.locationItems} onChange={this.handleLocation} />
                                    </Col>
                                    <Col>
                                        <button onClick={this.search} className="btn btn-info btn-sm" style={{ marginTop: 25 }}> <i className="fas fa-search" style={{ marginLeft: 8 }}></i></button>
                                    </Col>


                                </Row>

                            </Card.Header>
                            <Card.Body style={{ paddingBottom: 50, height: 500 }}>
                                {this.state.scheduleLoading ? (
                                    <Loading />
                                ) : (
                                        <ScheduleComponent events={this.state.events} onSelect={this.handleEvent} />
                                    )}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xs={12} style={{ display: 'none' }} >
                        <Card className='Recent-Users'>
                            <Card.Header>
                                <Card.Title as='h5'>{t('Upcoming')}</Card.Title>
                            </Card.Header>
                            <Card.Body className='px-0 py-2'>
                                <UpcomingList data={this.state.upcomingReservations} />
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xs={4}  style={{ display: 'none' }}>
                        <Card>
                            <Card.Body>
                                {this.state.salesLoading ? (
                                    <Loading />
                                ) : (
                                        <SalesComponent title={"Weekly Earnings"} total={this.state.sales.byWeek} symbol={"$"} />
                                    )}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xs={4}  style={{ display: 'none' }}>
                        <Card>
                            <Card.Body>
                                {this.state.salesLoading ? (
                                    <Loading />
                                ) : (
                                        <SalesComponent title={"Monthly Earnings"} total={this.state.sales.byMonth} symbol={"$"} />
                                    )}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={4}  style={{ display: 'none' }}>
                        <Card>
                            <Card.Body>

                                {this.state.salesLoading ? (
                                    <Loading />
                                ) : (
                                        <SalesComponent title={"Yearly Earnings"} total={this.state.sales.byYear} symbol={"$"} />
                                    )}
                            </Card.Body>
                        </Card>
                    </Col>


                    <Col md={6} xl={4} style={{ display: 'none' }}>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>Rating</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <div className="row align-items-center justify-content-center m-b-20">
                                    <div className="col-6">
                                        <h2 className="f-w-300 d-flex align-items-center float-left m-0">4.7 <i className="fa fa-star f-10 m-l-10 text-c-yellow" /></h2>
                                    </div>
                                    <div className="col-6">
                                        <h6 className="d-flex  align-items-center float-right m-0">0.4 <i className="fa fa-caret-up text-c-green f-22 m-l-10" /></h6>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xl-12">
                                        <h6 className="align-items-center float-left"><i className="fa fa-star f-10 m-r-10 text-c-yellow" />5</h6>
                                        <h6 className="align-items-center float-right">384</h6>
                                        <div className="progress m-t-30 m-b-20" style={{ height: '6px' }}>
                                            <div className="progress-bar progress-c-theme" role="progressbar" style={{ width: '70%' }} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" />
                                        </div>
                                    </div>

                                    <div className="col-xl-12">
                                        <h6 className="align-items-center float-left"><i className="fa fa-star f-10 m-r-10 text-c-yellow" />4</h6>
                                        <h6 className="align-items-center float-right">145</h6>
                                        <div className="progress m-t-30  m-b-20" style={{ height: '6px' }}>
                                            <div className="progress-bar progress-c-theme" role="progressbar" style={{ width: '35%' }} aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" />
                                        </div>
                                    </div>

                                    <div className="col-xl-12">
                                        <h6 className="align-items-center float-left"><i className="fa fa-star f-10 m-r-10 text-c-yellow" />3</h6>
                                        <h6 className="align-items-center float-right">24</h6>
                                        <div className="progress m-t-30  m-b-20" style={{ height: '6px' }}>
                                            <div className="progress-bar progress-c-theme" role="progressbar" style={{ width: '25%' }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" />
                                        </div>
                                    </div>

                                    <div className="col-xl-12">
                                        <h6 className="align-items-center float-left"><i className="fa fa-star f-10 m-r-10 text-c-yellow" />2</h6>
                                        <h6 className="align-items-center float-right">1</h6>
                                        <div className="progress m-t-30  m-b-20" style={{ height: '6px' }}>
                                            <div className="progress-bar progress-c-theme" role="progressbar" style={{ width: '10%' }} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" />
                                        </div>
                                    </div>
                                    <div className="col-xl-12">
                                        <h6 className="align-items-center float-left"><i className="fa fa-star f-10 m-r-10 text-c-yellow" />1</h6>
                                        <h6 className="align-items-center float-right">0</h6>
                                        <div className="progress m-t-30  m-b-5" style={{ height: '6px' }}>
                                            <div className="progress-bar" role="progressbar" style={{ width: '0%' }} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" />
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={8} className='m-b-30' style={{ display: 'none' }}>
                        <Tabs defaultActiveKey="today" id="uncontrolled-tab-example">
                            <Tab eventKey="today" title="Today">
                                {tabContent}
                            </Tab>
                            <Tab eventKey="week" title="This Week">
                                {tabContent}
                            </Tab>
                            <Tab eventKey="all" title="All">
                                {tabContent}
                            </Tab>
                        </Tabs>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(Dashboard);