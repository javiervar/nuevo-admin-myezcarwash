import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import logo from '../../../assets/images/logo2.png';
import logo3 from '../../../assets/images/logo3.png';

import CONSTANT from "../../../store/constant";


class UpcomingList extends React.Component {
    state = {}

    renderRows = () => {
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index} className="unread">

                    <td>
                        {row.SERVICE_TYPE == 1 ? (
                            <img className="rounded-circle" style={{ width: '40px' }} src={logo} alt="activity-user" />

                        ) : (
                                <img className="rounded-circle" style={{ width: '40px' }} src={logo3} alt="activity-user" />

                            )}

                    </td>
                    <td>
                        <h6 className="mb-1">{row.CLIENT}</h6>
                        <p className="m-0">{row.SERVICE_CATEGORY}({row.SERVICE_NAME})</p>
                        <p className="m-0">{row.LOCATION}</p>
                    </td>
                    <td>
                        <h6 className="text-muted"><i className="fa fa-circle text-c-green f-10 m-r-15" />{row.DATE}<br /><span style={{ paddingLeft: 25 }}>{row.HOUR_12}</span></h6>
                    </td>
                    <td>
                        <Link
                            className="label theme-bg text-white f-12"
                            to={{
                                pathname: "/reservations/detail",
                                state: [{ id: row.RESERVATION_ID }]
                            }}
                        >Details</Link>
                    </td>


                </tr>
            );
        })

        return rows


    }
    render() {
        return (
            <Table responsive hover>
                <tbody>
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default UpcomingList;