import React from 'react';
import { Card } from 'react-bootstrap';
import CONSTANT from "../../../store/constant";
import { Calendar, momentLocalizer, View } from 'react-big-calendar'
import * as moment from 'moment';
import "react-big-calendar/lib/css/react-big-calendar.css";


class ScheduleComponent extends React.Component {
    state={}
    

    render() {
        const localizer = momentLocalizer(moment)

        return (
           
                    <Calendar
                        localizer={localizer}
                        events={this.props.events}
                        views={{ month: true, day: true }}
                        startAccessor="start"
                        endAccessor="end"
                        onSelectEvent={this.props.onSelect}
                    />
               
        );
    }
}

export default ScheduleComponent;