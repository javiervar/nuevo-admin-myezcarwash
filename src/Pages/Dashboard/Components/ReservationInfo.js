import React from 'react';
import MapContainer from './MapContainer';
import { Link } from 'react-router-dom'

const ReservationInfo = (props) => {
    console.log(props.info)
    const { data } = props.info;
    var status = "Paid";
    const center = {
        lat: parseFloat(data[0].LAT),
        lng: parseFloat(data[0].LNG)
    };
    
    const zoom = 11;
    if (data[0].STATUS == 2) {
        status = "Taken";
    } else if(data[0].STATUS == 3){
        status = "Done";
    }
    const adds = data[0].ADDITIONALS.map((add) => {
        if (add.CHECK) {
            return (
                <li key={add.ADDITIONAL_ID}>
                    {add.ADDITIONAL_NAME}
                </li>
            )
        }

    })

    return (
        <div>
            <div style={{ textAlign: 'center' }}>
                <h3>{data[0].LOCATION}</h3>
                <h4>{data[0].SERVICE_CATEGORY}</h4>
                <h5>({data[0].SERVICE_NAME})</h5>
            </div>
            <div className="row" style={{ marginTop: 10 }}>
                <div className="col">

                    <h5>Client:</h5>
                    <p>
                        <b>Name: </b>{data[0].CUSTOMER_NAME} {data[0].CUSTOMER_LASTNAME}
                    </p>
                    <p>
                        <b>Phone:</b>{data[0].PHONE}
                    </p>
                    <p>
                        <b>Email:</b>{data[0].EMAIL}
                    </p>
                    <p>
                        <b>Address: </b> {data[0].ADDRESS}
                    </p>
                    <p>
                        <b>Vehicle: </b> {data[0].MAKE_NAME} {data[0].MODEL_NAME} {data[0].YEAR} {data[0].COLOR}
                    </p>
                    <p>
                        <b>Plate: </b> {data[0].PLATE}
                    </p>
                </div>
                <div className="col">
                    <h5>Service:</h5>
                    <p>
                        <b>Type: </b>{data[0].SERVICE_TYPE == 1 ? "Stationary" : "Mobile"}
                    </p>
                    <p>
                        <b>Reservation: </b>{data[0].RESERVATION_ID} 
                        
                    </p>
                    <p>
                        <b>Date: </b>{data[0].DATE} {data[0].HOUR_12}
                    </p>
                    <p>
                        <b>Status: </b>{status}
                    </p>
                    <p>
                        <b>Employee: </b>{data[0].EMPLOYEE}
                    </p>
                    <p>
                        <b>Additionals: </b>
                        {adds}
                    </p>
                </div>

            </div>
            {data[0].SERVICE_TYPE==2?(
            <div style={{height:400,width:"100%"}}>
                <MapContainer position={center} initialCenter={center} name={"client"}/>
                
            </div>
            ):null}
        </div>

    )
}



export default ReservationInfo;