import React from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import "./mapStyles.css"
export class MapContainer extends React.Component {
    render() {
        return (
            <Map google={this.props.google} className="map" zoom={14} initialCenter={this.props.initialCenter} style={{ height:"400px", width: '90%' }}>

                <Marker
                    name={this.props.name}
                    position={this.props.position}
                    title={this.props.title}
                />

               
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE")
})(MapContainer)