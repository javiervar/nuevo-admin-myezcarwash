import React from 'react';
import { Card } from 'react-bootstrap';
import CONSTANT from "../../../store/constant";


class SalesComponent extends React.Component {
    state = {}


    render() {

        return (
            <div>
                <h6 className='mb-4'>{this.props.title}</h6>
                <div className="row d-flex align-items-center">
                    <div className="col-9">
                        <h3 className="f-w-300 d-flex align-items-center m-b-0">
                            <i className="feather icon-arrow-up text-c-green f-30 m-r-5" /> {this.props.symbol}{this.props.total}
                            </h3>
                    </div>

                    
                </div>
                
            </div>
        );
    }
}

export default SalesComponent;