import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import TableComponent from './Components/TableComponent';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class AddAdditional extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            additional: '',
            price: 0,
            user:{}
        }
    }
    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user=JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user:user
            })
            console.log(user)
        }else{
            this.props.history.push("/");
        } 
       
    }

    componentDidMount() {
    }


    onFormSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        const { additional,price } = this.state;
        console.log(additional, price);
        var params = {
            additional: additional,
            price: price,
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addAdditional", params)
            .then((response) => {
                console.log(response)
                if (response.data.error == 0) {
                    alert("The additional was added successfully");
                    this.setState({
                        loading: false,
                        additional: '',
                        price: 0,
                    })
                } else {
                    alert(response.data.text)
                    this.setState({
                        loading: false,
                    });
                }
            });
    }




    render() {
        const{t}=this.props;
        return (
            <Aux>
                <Row>
                    <Col>


                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Add new additional")}</Card.Title>

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Form onSubmit={this.onFormSubmit}>
                                            <Form.Group >
                                                <Form.Label>{t("Additional")}</Form.Label>
                                                <Form.Control type="Text" name="additional"
                                                    value={this.state.additional}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                additional: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />

                                            </Form.Group>

                                            <Form.Group >
                                                <Form.Label>Price </Form.Label>
                                                <Form.Control type="number" name="price"
                                                    value={this.state.price}
                                                    onChange={(e) => {
                                                        this.setState(
                                                            {
                                                                price: e.target.value
                                                            })
                                                    }}
                                                    required
                                                />
                                            </Form.Group>

                                            <Button variant="primary" type="submit">
                                                {t("Submit")}
                                            </Button>
                                        </Form>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(AddAdditional);