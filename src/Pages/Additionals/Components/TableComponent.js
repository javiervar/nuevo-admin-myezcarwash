import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const{t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{row.ADDITIONAL_ID}</th>
                    <td>{row.ADDITIONAL_NAME}</td>
                    <td>{row.PRICE}</td>
                    <td>
                        {row.STATUS==1?'Active':'Disabled'}
                    </td>
                    
                    <td>

                        <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/services/service",
                                state: [{ company: row.SERVICE_ID }]
                            }}
                        >{t("Details")}</Link>
                    </td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props;
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">
                    <tr>
                        <th>Id</th>
                        <th>{t("Name")}</th>
                        <th>{t("Price")}</th>
                        <th>{t("Status")}</th>
                        <th>{t("Command")}</th>
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);