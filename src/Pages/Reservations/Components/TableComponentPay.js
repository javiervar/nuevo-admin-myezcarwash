import React from 'react';
import { Table, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = { adds: null }

    changeStatus = (res) => {

        console.log("res", res);
        res.check = res.check ? false : true;

        console.log("cambio", this.props.data);
    }
    renderRows = () => {
        const rows = this.props.data.map((row, index) => {
            row.check = true;
            return (
                <tr key={index}>
                    <th style={{ textAlign: 'center' }}>
                        <Form.Check
                            defaultChecked

                            type={'checkbox'}
                            onClick={(e) => {
                                this.changeStatus(row);
                            }}
                        />
                    </th>
                    <th scope="row">
                        <Link
                            className="btn btn-link"
                            to={{
                                pathname: "/reservations/detail",
                                state: [{ id: row.RESERVATION_ID }]
                            }}
                        >{row.RESERVATION_ID}</Link>
                    </th>
                    <td>{row.SERVICE_TYPE === '1' ? 'Stationary' : 'Mobile'}</td>
                    <td>{row.STATUS === '1' ? 'PAID' : row.STATUS === '2' ? 'TAKEN' : 'DONE'}</td>
                    <td>{row.DATE} {row.HOUR_12}</td>
                    <td>{row.LOCATION}</td>
                    <td>{row.RETAILER}</td>
                    <td>{row.SERVICE_NAME}</td>
                    <td>{row.CUSTOMER_NAME} {row.CUSTOMER_LASTNAME}</td>
                    <td>{row.PHONE}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.MAKE_NAME} {row.MODEL_NAME} {row.YEAR} {row.COLOR}</td>
                    <td>{row.PLATE}</td>
                    <td>
                        {row.ADDITIONALS.length > 0 ? (
                            row.ADDITIONALS.map((head, index) => {
                                if (head.CHECK) {
                                    return <span key={index}>{head.ADDITIONAL_NAME},</span>
                                }
                            })
                        ) : (<span>None</span>)}

                    </td>
                    <td>{row.TOTAL}</td>
                    <td>{row.TIP > 0 ? row.TIP : "0.00"}</td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const { t } = this.props;

        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>{t("Pay")}</th>
                        <th>Id</th>
                        <th>{t("Category")}</th>
                        <th>{t("Status")}</th>
                        <th>{t("Date")}</th>
                        <th>{t("Location")}</th>
                        <th>Retailer</th>
                        <th>{t("Service")} </th>
                        <th>{t("Customer")}</th>
                        <th>{t("Phone")}</th>
                        <th>{t("E-MAIL")}</th>
                        <th>{t("Vehicle")}</th>
                        <th>{t("Plate")}</th>
                        <th>Adds</th>
                        <th>Total</th>
                        <th>{t("Tip")}</th>
                    </tr>
                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);