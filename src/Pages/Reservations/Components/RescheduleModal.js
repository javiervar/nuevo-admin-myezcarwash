import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import DayPicker from 'react-day-picker';
import { Modal, Button } from 'react-bootstrap';
import Select from '../../Reports/Components/Select';
import 'react-day-picker/lib/style.css';
import axios from 'axios';
import CONSTANT from '../../../store/constant';


class RescheduleModal extends React.Component {
    state = { selected: null, timeItem: [] }

    handleDayClick = (date, { selected }) => {
        console.log(date.toISOString().slice(0,10))
        this.setState({
            selectedDay: selected ? undefined : date,
        });
        

        var postData = {
            params: {
                location: this.props.locationId,
                date:date.toISOString().slice(0,10)
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRlIjoiMjAxOVwvMDZcLzEyIiwidHlwZSI6IjMiLCJpZCI6IjMyIiwiZW1haWwiOiJlZGdhci52YWxkZXpyQGdtYWlsLmNvbSJ9.7R_ekvoTF2TtwFhz4V7qKOOxLLZHxigLw1FiCNPRJ3s';

        axios.get(CONSTANT.API_PATH + "/getScheduleItem", postData)
            .then((response) => {
                console.log(response);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }
    handleSelect = (val) => {
        console.log(val);
    }
    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.onHide}>
                <Modal.Header closeButton>
                    <Modal.Title>Reschedule</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <DayPicker
                        initialMonth={new Date()}
                        selectedDays={this.state.selectedDay}
                        onDayClick={this.handleDayClick}
                        disabledDays={[{ before: new Date() }, { daysOfWeek: this.props.disabled }]}
                    />

                </Modal.Body>
                <Select label="Time :" options={this.state.timeItem} onChange={this.handleSelect} />
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.props.onHide}>
                        Close
          </Button>

                </Modal.Footer>
            </Modal>
        );
    }
}

export default RescheduleModal;