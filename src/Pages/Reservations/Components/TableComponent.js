import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = { adds: null }

    renderRows = () => {
        const { t } = this.props;
        const rows = this.props.data.map((row, index) => {
            var status = "";
            switch (row.STATUS) {
                case '1':
                    status = "PAID";
                    break;
                case '2':
                    status = "TAKEN";
                    break;
                case '3':
                    status = "DONE";
                    break;
                case '4':
                    status = "CANCELED";
                    break;
                case '5':
                    status = "PAID RET";
                    break;
            }
            return (
                <tr key={index}>

                    <th >
                        <Link
                            className="btn btn-info btn-sm"
                            to={{
                                pathname: "/reservations/detail",
                                state: [{ id: row.RESERVATION_ID }]
                            }}
                        >{t("Details")}</Link>
                        {row.STATUS <= 3 ? (
                            <Button size="sm" variant="danger" onClick={()=>this.props.cancel(row.RESERVATION_ID)}>{t("Cancel")}</Button>
                        )
                            : null
                        }
                    </th>
                    <th scope="row">
                        <Link
                            className="btn btn-link"
                            to={{
                                pathname: "/reservations/detail",
                                state: [{ id: row.RESERVATION_ID }]
                            }}
                        >{row.RESERVATION_ID}</Link>
                    </th>
                    <td>{row.SERVICE_TYPE === '1' ? 'Stationary' : 'Mobile'}</td>
                    <td>{status}</td>
                    <td>{row.DATE} {row.HOUR_12}</td>
                    <td>{row.LOCATION}</td>
                    <td>{row.SERVICE_NAME}</td>
                    <td>{row.CUSTOMER_NAME} {row.CUSTOMER_LASTNAME}</td>
                    <td>{row.PHONE}</td>
                    <td>{row.EMAIL}</td>
                    <td>{row.MAKE_NAME} {row.MODEL_NAME} {row.YEAR} {row.COLOR}</td>
                    <td>{row.PLATE}</td>
                    <td>
                        {row.ADDITIONALS.map((head, index) => {
                            if (head.CHECK) {
                                return <span key={index}>{head.ADDITIONAL_NAME},</span>
                            }
                        })}
                    </td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const { t } = this.props;

        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>{t("Command")}</th>
                        <th>Id</th>
                        <th>{t("Category")}</th>
                        <th>{t("Status")}</th>
                        <th>{t("Date")}</th>
                        <th>{t("Location")}</th>
                        <th>{t("Service")} </th>
                        <th>{t("Customer")}</th>
                        <th>{t("Phone")}</th>
                        <th>{t("E-MAIL")}</th>
                        <th>{t("Vehicle")}</th>
                        <th>{t("Plate")}</th>
                        <th>Adds</th>
                    </tr>
                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);