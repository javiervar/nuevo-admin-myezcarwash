import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import RescheduleModal from './Components/RescheduleModal';
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import MapContainer from '../Dashboard/Components/MapContainer';

class ReservationDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            showModal: false,
            disabledDays: [],
            location: -1,
            user: {}
        }
    }

    componentDidMount() {
        this.getInfo();
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    getInfo = () => {
        this.setState({ loading: true })
        var postData = {
            params: {
                reservation: this.props.location.state[0].id,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getReservationInfo", postData)
            .then((response) => {
                console.log(response);
                this.setState({
                    loading: false,
                    data: response.data.data[0],
                    location: response.data.data[0].LOCATION_ID
                })
                this.getLocationWorkDays(response.data.data[0].LOCATION_ID)
            })
            .catch((error) => {
                console.log(error);
            })
    }




    renderAdds(data) {
        var ret = [];
        for (var add of data.ADDITIONALS) {
            if (add.CHECK) {
                ret.push(
                    <li key={add.ADDITIONAL_ID}>
                        {add.ADDITIONAL_NAME}
                    </li>
                )
            }
        }

        if (ret.length == 0) {
            return (
                <span>
                    No adds
                </span>
            );
        }

        return ret;
    }

    renderStatus(status) {
        var ret = "Paid";
        if (status == 2) {
            ret = "Taken";
        } else if (status == 3) {
            ret = "Done"
        }

        return ret;
    }

    getLocationWorkDays = (location) => {

        var postData = {
            params: {
                location: location,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getWorkDays", postData)
            .then((response) => {
                var disabled = []
                for (var i = 0; i <= 6; i++) {
                    var flag = false;
                    for (var x of response.data.data) {
                        if (i == x.DAY_ID)
                            flag = true;
                    }
                    if (!flag)
                        disabled.push(i)
                }
                console.log("disabled", disabled)
                this.setState({
                    loading: false,
                    disabledDays: disabled,
                })
            })
            .catch((error) => {
                console.log(error);
            })

    }

    handleClick = () => {
        this.handleModal();
    }

    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }
    renderMap(data) {
        const center = {
            lat: parseFloat(data.LAT),
            lng: parseFloat(data.LNG)
        };
        console.log("center", center)
        return (
            <div style={{ height: 400, width: "100%" }}>
                <MapContainer position={center} initialCenter={center} name={"client"} />

            </div>
        )
    }

    render() {
        const { data } = this.state;
        return (
            <Aux>
                <RescheduleModal show={this.state.showModal} onHide={this.handleModal} disabled={this.state.disabledDays} locationId={this.state.location} />

                <Row>

                    <Col>

                        {this.state.loading ? (
                            <Loading />
                        ) : (
                                <Card>
                                    <Card.Header>
                                        <Card.Title as="h5">Reservation # {data.RESERVATION_ID}</Card.Title>
                                        <br></br><br></br>
                                        <Button variant="primary" className="btn-sm" onClick={this.handleClick}><i class="far fa-calendar-alt"></i> Reschedule</Button>

                                    </Card.Header>
                                    <Card.Body>
                                        <Row style={{ paddingBottom: 5, borderBottom: "1px solid #ddd" }}>
                                            <Col md={12}>
                                                <h5>Client:</h5>
                                            </Col>
                                            <Col md={6}>


                                                <p>
                                                    <b>Name: </b>{data.CUSTOMER_NAME} {data.CUSTOMER_LASTNAME}
                                                </p>
                                                <p>
                                                    <b>Phone:</b>{data.PHONE}
                                                </p>
                                                <p>
                                                    <b>Email:</b>{data.EMAIL}
                                                </p>
                                            </Col>
                                            <Col md={6}>


                                                <p>
                                                    <b>Address: </b> {data.ADDRESS}
                                                </p>
                                                <p>
                                                    <b>Vehicle: </b> {data.MAKE_NAME} {data.MODEL_NAME} {data.YEAR} {data.COLOR}
                                                </p>
                                                <p>
                                                    <b>Plate: </b> {data.PLATE}
                                                </p>
                                            </Col>
                                        </Row>
                                        <Row style={{ paddingBottom: 5, paddingTop: 5, borderBottom: "1px solid #ddd" }}>
                                            <Col md={12}>
                                                <h5>Employee info:</h5>
                                            </Col>
                                            <Col md={12}>

                                                {data.STATUS != '1' ? (
                                                    <div>
                                                        <p>
                                                            <b>Id: </b>
                                                            <Link
                                                                to={{
                                                                    pathname: "/employees/detail",
                                                                    state: [{ id: data.EMPLOYEE }]
                                                                }}
                                                            >{data.EMPLOYEE}</Link>
                                                        </p>
                                                        <p>
                                                            <b>User name: </b>{data.EMPLOYEE_USER}
                                                        </p>
                                                        <p>
                                                            <b>Name: </b>{data.EMPLOYEE_NAME} {data.EMPLOYEE_LASTNAME}
                                                        </p>

                                                    </div>

                                                ) : (
                                                        <p>
                                                            <b>Not taken yet</b>
                                                        </p>
                                                    )}

                                            </Col>
                                        </Row>
                                        <Row style={{ paddingBottom: 5, paddingTop: 5, borderBottom: "1px solid #ddd" }}>
                                            <Col md={12}>
                                                <h5>Transaction info:</h5>
                                            </Col>
                                            <Col md={12}>
                                                <p>
                                                    <b>TAX: </b>%{data.TAX}
                                                </p>
                                                <p>
                                                    <b>Service price: </b>${data.PRICE}
                                                </p>
                                                <p>
                                                    <b>Total Adds: </b>${data.TOTAL_ADDITIONAL}
                                                </p>
                                                <p>
                                                    <b>Promo code: </b>{data.CODE==null?"None":data.CODE}
                                                </p>
                                                <p>
                                                    <b>Promo discount: </b>${data.PROMO_DISCOUNT==null?"0":data.PROMO_DISCOUNT}
                                                </p>
                                                <p>
                                                    <b>Total: </b>${data.TOTAL}
                                                </p>
                                                <p>
                                                    <b>TIP: </b>${data.TIP==null?"0":data.TIP}
                                                </p>
                                            </Col>
                                        </Row>
                                        <Row style={{ paddingBottom: 5, paddingTop: 5 }}>
                                            <Col md={12}>
                                                <h5>Service:</h5>
                                            </Col>
                                            <Col md={6}>
                                                <p>
                                                    <b>Reservation: </b>{data.RESERVATION_ID}
                                                </p>
                                                <p>
                                                    <b>Category: </b>{data.SERVICE_TYPE == 1 ? "Stationary" : "Mobile"}
                                                </p>
                                                <p>
                                                    <b>Location: </b> {data.LOCATION}
                                                </p>
                                                <p>
                                                    <b>Status: </b>
                                                    {this.renderStatus(data.STATUS)}
                                                </p>
                                               


                                            </Col>
                                            <Col md={6}>
                                                <p>
                                                    <b>Date: </b>{data.DATE} {data.HOUR_12} <br />
                                                </p>
                                               
                                                <p>
                                                    <b>Service: </b> {data.SERVICE_CATEGORY} ({data.SERVICE_NAME})
                                                </p>
                                                <p>
                                                    <b>Additionals: </b>
                                                    {
                                                        this.renderAdds(data)
                                                    }
                                                </p>


                                            </Col>
                                        </Row>
                                        <Row style={{ paddingBottom: 5, paddingTop: 5 }}>
                                            <Col>
                                                {data.SERVICE_TYPE == 2 ? (
                                                    this.renderMap(data)
                                                ) : null}
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            )}

                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default ReservationDetail;