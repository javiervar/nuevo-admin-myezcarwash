import React from 'react';
import { Row, Col, Card, Form, Button, Modal } from 'react-bootstrap';
import './Reports.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import TableComponent from './Components/TableComponentPay';
import Loading from "../UIElements/Basic/Loading"
import ReactPaginate from 'react-paginate';
import Select from '../Reports/Components/Select';
import CONSTANT from '../../store/constant';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
const selecteData = {
    opc_status: [
        {
            name: 'ALL',
            value: -1
        },
        {
            name: 'PAID',
            value: 1
        },
        {
            name: 'TAKEN',
            value: 2,
        }, {
            name: 'DONE',
            value: 3
        }

    ],
    opc_service: [
        {
            name: 'STATIONARY',
            value: 1
        },
        {
            name: 'MOBILE',
            value: 2
        }
    ],
    opc_search: [
        {
            name: 'ALL',
            value: 0
        },
        {
            name: 'EMPLOYEE',
            value: 2
        },
        {
            name: 'RETAILER',
            value: 3
        },
        {
            name: 'LOCATION',
            value: 4
        }


    ]
};
class PayReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pages: 0,
            adds: [],
            data: [],
            employeeId: -1,
            currentPage: 1,
            loading: false,
            opc_service: [],
            opc_search: [],
            searchBy: 0,
            searviceType: 1,
            dateFrom: 0,
            dateTo: 0,
            term: 0,
            status: -1,
            user: {},
            showModal: false,
            toPay: []
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }

    componentDidMount() {
        //this.searchData(0);

    }
    searchData = (page) => {
        this.setState({ loading: true, currentPage: page })
        const { searviceType, searchBy, dateFrom, dateTo, term, status, employeeId } = this.state;
        var option = 1;
        var dateToTemp = dateTo;
        if (dateFrom != 0) {
            option = 2;
            if (dateToTemp == 0) {
                dateToTemp = new Date().toJSON().slice(0, 10);
            }
        }


        var postData = {
            params: {
                opc: option,
                employeeId: employeeId,
                from: dateFrom,
                to: dateToTemp,
                service: searviceType,
                page: page,
                by: searchBy,
                txt: term,
                status: status,
                retailer: this.state.user.type == '2' ? this.state.user.id : -1,
                paid: 1,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAllReservations", postData)
            .then((response) => {
                console.log(response);
                this.setState({ data: response.data.data, pages: response.data.pages, adds: response.data.adds, loading: false })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };

    handleServiceType = val => {
        console.log('servicetype ', val)
        this.setState({ searviceType: val })
    }
    handleSearchBy = val => {
        console.log('searchby ', val)
        this.setState({ searchBy: val })
    }

    handleStatus = val => {
        console.log('status ', val)
        this.setState({ status: val })
    }

    handleDateFrom = val => {
        console.log('datefrom ', val)
        this.setState({ dateFrom: val.target.value })
    }

    handleDateTo = val => {
        console.log('dateto ', val)
        this.setState({ dateTo: val.target.value })
    }

    pay = () => {

        const { data } = this.state;
        var copy = [...data];
        for (var i in copy) {
            if (!copy[i].check)
                copy.splice(i, 1)
        }

        console.log("datos cambiados->", copy)
        this.setState({
            toPay: copy
        }, () => {
            this.handleModal();
        })
        //var index = data.length;
        //this.saveStatusPaid(data, index - 1);
    }
    confirmPay = () => {
        let { toPay } = this.state;
        var index = toPay.length;
        this.saveStatusPaid(toPay, index - 1);

    }
    //recursivity save services
    saveStatusPaid = (data, index) => {

        var params = data[index];
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/updateRetailerPaid", params)
            .then((response) => {

                if (index > 0)
                    this.saveStatusPaid(data, index - 1);
                else {

                    this.searchData(0);
                    this.handleModal();
                    toast.success(data.length + " reservations were successfully paid", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                }

            });
    }
    handleModal = () => {
        const { showModal } = this.state;

        this.setState({
            showModal: !showModal
        })
    }
    render() {
        const { t } = this.props;

        return (
            <Aux>
                <ToastContainer
                    hideProgressBar
                    newestOnTop={false}
                />
                <Modal show={this.state.showModal} onHide={this.handleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you pay the following reservations?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>

                            {this.state.toPay.map((val, index) => {
                                return (
                                    <Col xs={2} key={index}>
                                        {val.RESERVATION_ID}
                                    </Col>
                                );
                            })}
                        </Row>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="success" onClick={this.confirmPay}>
                            Confirm
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Row>
                    <Col>
                        <Card>

                            <Card.Body>
                                <Row>
                                    <Col>
                                        <Select label="Search by:" options={selecteData.opc_search} onChange={this.handleSearchBy} />
                                    </Col>
                                    <Col xs={8}>
                                        <Form.Control
                                            type="text"
                                            placeholder="Search"
                                            style={{ marginTop: 18 }}
                                            disabled={this.state.searchBy == 0 ? true : false}
                                            onChange={(e) => { this.setState({ term: e.target.value }) }}
                                        />
                                    </Col>
                                    <Col>
                                        <Button variant="info" style={{ marginTop: 18 }} onClick={() => this.searchData(0)}>
                                            <span className="fas fa-search"></span>
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>

                                    <Col>
                                        <Select label="Service type:" options={selecteData.opc_service} onChange={this.handleServiceType} />
                                    </Col>

                                    <Col>
                                        <Select label="Satus:" options={selecteData.opc_status} onChange={this.handleStatus} />
                                    </Col>

                                    <Col>
                                        <Form.Group >
                                            <Form.Label>From</Form.Label>
                                            <Form.Control size="sm" type="date" onChange={this.handleDateFrom} />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group>
                                            <Form.Label>To</Form.Label>
                                            <Form.Control size="sm" type="date" onChange={this.handleDateTo} />
                                        </Form.Group>
                                    </Col>


                                </Row>


                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Reservations")}</Card.Title>
                                <br>
                                </br>
                                {this.state.data.length > 0 ? (
                                    <Button variant="success" size="sm" onClick={() => this.pay()}>
                                        {t("Pay")}
                                </Button>
                                ) : null
                                }

                            </Card.Header>
                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (this.state.data.length > 0 ? (
                                    <div>

                                        <TableComponent data={this.state.data} adds={this.state.adds} />

                                        <ReactPaginate
                                            previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                            nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}

                                            forcePage={this.state.currentPage}
                                            pageCount={this.state.pages}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={'pagination'}
                                            subContainerClassName={'pages pagination'}
                                            activeClassName={'active'}
                                        />
                                    </div>) : (
                                        <div className='noResults'>
                                            <span>{t("No results")}</span>
                                        </div>
                                    )
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(PayReservation);