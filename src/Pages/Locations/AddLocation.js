import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import CONSTANT from '../../store/constant';
import FormInfoStep from './Steps/FormInfoStep';
import DrawingMapStep from './Steps/DrawingMapStep';
import ScheduleStep from './Steps/ScheduleStep';
import StepWizard from 'react-step-wizard';
import WorksDayStep from './Steps/WorksDayStep';
import ServicesStep from './Steps/ServicesStep';
import MapPositionStep from './Steps/MapPositionStep';

import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class AddLocations extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            retailers: [],
            services: [],
            serviceType: 1,
            user: {},
            hours: [],
            timeTo: 18,
            timeFrom: 8,
            timeFromTo: [],
            step1: undefined,
            step2: undefined,
            step3: undefined,
            step4: undefined,
            step5: undefined,
            step5_m: undefined
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user,
            }, () => {

                this.getGeneratedTime();
                this.getTimeFrom();
                this.getRetailers();
                this.getServices();

            })
        } else {
            this.props.history.push("/");
        }
    }

    getTimeFrom() {
        this.setState({
            loading: true
        })

        var postData = {
            params: {
                from: 8,
            }
        };

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getTimesItem", postData)
            .then((response) => {
                this.setState({
                    loading: false,
                    hours: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })

    }

    getGeneratedTime() {
        var postData = {
            params: {
                from: this.state.timeFrom,
                to: this.state.timeTo
            }
        };

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getTimesFromTo", postData)
            .then((response) => {
                this.setState({
                    loading: false,
                    timeFromTo: response.data.data,
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getRetailers() {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getRetailerItems")
            .then((response) => {
                this.setState({
                    loading: false,
                    retailers: response.data.data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getServices() {
        var postData = {
            params: {
                serviceType: this.state.serviceType,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getServicesItem", postData)
            .then((response) => {
                var data = response.data.data;
                for (var service of data) {
                    service.STATUS = 1;
                    for (var add of service.ADDITIONALS) {
                        add.STATUS = 1;
                    }
                    var desc = JSON.parse(service.DESCRIPTION);
                    var temp = "";
                    for (var n in desc) {
                        temp += desc[n] + ", ";
                    }
                    service.DESCRIPTION = temp;
                }
                this.setState({
                    loading: false,
                    services: data,

                })
            })
            .catch((error) => {
                console.log(error);
            })
    }


    saveLocation = () => {
        this.setState({ loading: true });
        const { step1 } = this.state;

        var params = step1;
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addLocation", params)
            .then((response) => {
                var data = response.data;
                if (data.error == 0) {
                    this.toSaveStep2(data.data.id);
                    this.toSaveStep3(data.data.id);
                    this.toSaveStep4(data.data.id);
                    this.toSaveStep5(data.data.id);
                } else {
                    alert("Error")
                }

            });
    }
    //to save services
    toSaveStep2 = (locationId) => {
        const { services } = this.state;
        var index = services.length;
        this.saveStep2(services, index - 1, locationId);
    }
    toSaveStep3 = (locationId) => {
        const { step3 } = this.state;
        var days = [];
        if (step3.sun) {
            days.push(0);
        }
        if (step3.mon) {
            days.push(1);
        }
        if (step3.tue) {
            days.push(2);
        }
        if (step3.wed) {
            days.push(3);
        }
        if (step3.thu) {
            days.push(4);
        }
        if (step3.fri) {
            days.push(5);
        }
        if (step3.sat) {
            days.push(6);
        }

        var index = days.length;
        this.saveStep3(locationId, days, index - 1);


    }
    toSaveStep4 = (locationId) => {
        const { step4 } = this.state;
        var index = step4.length;
        this.saveStep4(step4, index - 1, locationId);
    }

    toSaveStep5 = (locationId) => {
        const { step5 } = this.state;
        console.log('step 5 :', step5);
        this.saveStep5(locationId, step5);
    }


    //recursivity save services
    saveStep2 = (services, index, locationId) => {
        var params = services[index];
        params.LOCATION_ID = locationId;
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addService", params)
            .then((response) => {
                var LS_ID = response.data.data.id;
                var additionals = params.ADDITIONALS;
                var indexAdds = additionals.length;
                if (indexAdds > 0)
                    this.saveStep2Adds(additionals, indexAdds - 1, LS_ID, params.SERVICE_ID);
                if (index > 0)
                    this.saveStep2(services, index - 1, locationId);
            });
    }
    //recursivity save additional of services
    saveStep2Adds = (additionals, index, LS_ID, serviceId) => {
        var add = additionals[index];
        var params = {
            ADDITIONAL_ID: add.ADDITIONAL_ID,
            SERVICE_ID: serviceId,
            LS_ID: LS_ID,
            STATUS: add.STATUS
        }
        console.log("ADD ",params)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addAdditionalService", params)
            .then((response) => {
                if (index > 0) {
                    this.saveStep2Adds(additionals, index - 1, LS_ID, serviceId);
                }else{
                    alert("Location added");
                    this.setState({
                        loading: false,
                    })
                }
            });
    }

    saveStep3 = (locationId, days, index) => {
        var params = {
            location: locationId,
            day: days[index],
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addWorkDay", params)
            .then((response) => {
                if (index > 0)
                    this.saveStep3(locationId, days, index - 1);

            });
    }
    saveStep4 = (scheduleLocation, index, locationId) => {
        var params = scheduleLocation[index];
        params.LOCATION_ID = locationId;

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addScheduleLocation", params)
            .then((response) => {
                if (index > 0)
                    this.saveStep4(scheduleLocation, index - 1, locationId);

            });
    }

    saveStep5 = (locationId, coors) => {
        var params = {
            location: locationId,
            lat: coors.lat,
            lng: coors.lng
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/updateCoordinates", params)
            .then((response) => {
                console.log(response);
               
            });
    }





    handleFormData = data => {
        this.setState({
            step1: data,
        })
        
    }
    handleDaysOfWork = data => {
        console.log(data)

        this.setState({
            step3: data,
        })
    }

    handleMaxCar = data => {
        console.log(data)
        this.setState({
            step4: data,
        })
    }

    handleServicesData = data => {
        this.setState({
            step2: this.state.services,
        })
    }

    handleCoordinates = data => {
        this.setState({
            step5: data,
        })
        //To save
        this.saveLocation();
    }

    handlePolygon = cors => {
        this.setState({
            step5: cors,
        })
        //To save
        this.saveLocation();
    }

    handleFromTime = val => {
        this.setState({
            timeFrom: val
        }, () => {
            this.getGeneratedTime()
        })
    }

    handleToTime = val => {
        this.setState({
            timeTo: val
        }, () => {
            this.getGeneratedTime()
        })
    }


    render() {
        const{t}=this.props;
        return (
            <Aux>

                <Card>
                    <Card.Header>
                        <Card.Title as="h5">{t("Add new location")}</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        {this.state.loading ? (
                            <Loading />
                        ) : (

                                <StepWizard>
                                    <FormInfoStep handleData={this.handleFormData} retailers={this.state.retailers} />
                                    <ServicesStep handleData={this.handleServicesData} services={this.state.services} />
                                    <WorksDayStep handleData={this.handleDaysOfWork} />
                                    <ScheduleStep handleData={this.handleMaxCar} schedule={this.state.timeFromTo} options={this.state.hours} handleFromTime={this.handleFromTime} handleToTime={this.handleToTime} />
                                    <MapPositionStep handleData={this.handleCoordinates} />

                                    {this.state.serviceType == 2 ? (
                                        <DrawingMapStep handlePolygon={this.handlePolygon} handleErasePolygon={this.handleErasePolygon} />
                                    ) : (<div></div>)
                                    }

                                </StepWizard>
                            )}
                    </Card.Body>
                </Card>
            </Aux>
        );
    }
}

export default withNamespaces()(AddLocations);