import React from 'react';
import { Row, Col, Form, Button, Table } from 'react-bootstrap';
import '../Style/Style.css'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
class WorksDayTab extends React.Component {
    state = {
    }

    handleNext = () => {
        this.props.handleData();
    }

    render() {
        const{t}=this.props;
        return (
            <div>


                <Row className="justify-content-md-center">
                    <Col xs={12} md={6}>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>{t("SUN")}</th>
                                    <th>{t("MON")} </th>
                                    <th>{t("TUE")}</th>
                                    <th>{t("WED")}</th>
                                    <th>{t("THU")}</th>
                                    <th>{t("FRI")}</th>
                                    <th>{t("SAT")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.sun}
                                            onClick={(e) => {
                                                this.props.days.sun = e.target.checked

                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.mon}
                                            onClick={(e) => {
                                                this.props.days.mon = e.target.checked
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.tue}
                                            onClick={(e) => {
                                                this.props.days.tue = e.target.checked

                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.wed}
                                            onClick={(e) => {
                                                this.props.days.wed = e.target.checked
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.thu}
                                            onClick={(e) => {
                                                this.props.days.thu = e.target.checked
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.fri}
                                            onClick={(e) => {
                                                this.props.days.fri = e.target.checked
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}
                                            defaultChecked={this.props.days.sat}
                                            onClick={(e) => {
                                                this.props.days.sat = e.target.checked

                                            }}
                                        />
                                    </td>


                                </tr>
                            </tbody>
                        </Table>

                    </Col>
                </Row>
                <Row className="controller">

                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }}>{t("Update")}</Button>
                    </Col>
                </Row>
            </div >
        );
    }
}

export default withNamespaces()(WorksDayTab);