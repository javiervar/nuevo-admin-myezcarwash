import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import Select from '../../Reports/Components/Select';
import '../Style/Style.css'
import Loading from "../../UIElements/Basic/Loading"
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class InfoTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }



    handleSubmit = (e) => {
        e.preventDefault();
        this.props.handleData();

    }


    handleServiceType = val => {
        console.log('servicetype ', val)
        this.setState({ serviceType: val })
    }
    handleRetailer = val => {
        console.log('retailer ', val)
        this.setState({ retailer: val })
    }
    render() {
        const{t}=this.props;
        return (
            <div>

                <Row>
                    <Col>
                        {this.props.info == {} ? (
                            <Loading />
                        ) : (
                                <Form onSubmit={this.handleSubmit}>

                                    <Form.Group size="sm">
                                        <Form.Label>{t("Retailer")}: </Form.Label>
                                        <Form.Control type="text" name="retailer"
                                            defaultValue={this.props.info.RETAILER}
                                            readOnly
                                        />
                                    </Form.Group>

                                    <Form.Group size="sm">
                                        <Form.Label>{t("Location Name")}: </Form.Label>
                                        <Form.Control type="text" name="name"
                                            defaultValue={this.props.info.LOCATION_NAME}
                                            onChange={(e) => {
                                                this.props.info.LOCATION_NAME = e.target.value
                                            }}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group size="sm">
                                        <Form.Label>{t("ZIP")}: </Form.Label>
                                        <Form.Control type="number" name="zip"
                                            defaultValue={this.props.info.ZIP}
                                            onChange={(e) => {
                                                this.props.info.ZIP = e.target.value

                                            }}
                                            minLength={4}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group size="sm">
                                        <Form.Label>{t("TAX")}: </Form.Label>
                                        <Form.Control type="number" name="tax"
                                            defaultValue={this.props.info.TAX}
                                            onChange={(e) => {
                                                this.props.info.TAX = e.target.value
                                            }}
                                            min={0}
                                            required
                                        />
                                    </Form.Group>

                                    <Row className="controller">
                                        <Col>
                                            <Button type="submit" style={{ float: 'right' }}>{t("Update")}</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            )}
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(InfoTab);