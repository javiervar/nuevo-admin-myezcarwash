import React from 'react';
import { Map, Polygon, Marker, GoogleApiWrapper } from 'google-maps-react';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
export class PositionTab extends React.Component {
    state = {
        coordinates: {},
        currentLocation: {
            lat: 30.1229035,
            lng: -95.4306791,
        }
    }
    componentWillMount() {

    }
    

    handleNext = () => {
        this.props.handleData(this.state.coordinates);
    }

    render() {
        const{t}=this.props;
        console.log(this.props.locationArea)
        const triangleCoords = [
            {lat: 25.774, lng: -80.190},
            {lat: 18.466, lng: -66.118},
            {lat: 32.321, lng: -64.757},
            {lat: 25.774, lng: -80.190}
          ];
        return (
            <div>
                <div className="title">
                    <h3>{t("Service area")}:</h3>
                </div>

                <div style={{ width: '100%', height: 400 }}>

                    <Map google={this.props.google} className="map" zoom={14} initialCenter={this.state.currentLocation} style={{ height: "400px", width: '90%' }}>
                        <Polygon
                            paths={this.props.locationArea}
                            strokeColor="#0000FF"
                            strokeOpacity={0.8}
                            strokeWeight={2}
                            fillColor="#0000FF"
                            fillOpacity={0.35} />
                    </Map>
                </div>
                <Row className="controller">

                    <Col>
                        <Link
                            className="btn btn-primary"
                            style={{ float: 'right' }}
                            to={{
                                pathname: "/locations/editDrawing",
                                state: [{ locationId: this.props.locationId,user:this.props.user }]
                            }}
                        >{t("Edit")}</Link>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(GoogleApiWrapper({
    apiKey: ("AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE")
})(PositionTab))