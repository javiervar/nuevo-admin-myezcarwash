import React from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import { Row, Col, Button } from 'react-bootstrap';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
export class PositionTab extends React.Component {
    state = {
        coordinates:{},
        currentLocation: {
            lat: 30.1229035,
            lng: -95.4306791,
        }
    }
    componentWillMount() {

    }
    onMarkerDragEnd = (coords) => {
        console.log(coords)
        const { latLng } = coords;
        const lat = latLng.lat();
        const lng = latLng.lng();
        const coord = {
            lat: lat,
            lng: lng
        }
        console.log(coord)
        this.setState({
            coordinates:coord
        })
    }

    handleNext = () => {
        this.props.handleData(this.state.coordinates);
    }

    render() {
        const{t}=this.props;
        console.log(this.props.currentLocation)
        return (
            <div>
                <div className="title">
                    <h3>{t("Location")} :</h3>
                </div>
                <h5>{t("Drag the marker to the center of the location.")}</h5>

                <div style={{ width: '100%', height: 400 }}>

                    <Map google={this.props.google} className="map" zoom={14} initialCenter={this.state.currentLocation} style={{ height: "400px", width: '90%' }}>
                        <Marker
                            name={'Current Location'}
                            position={this.props.currentLocation}
                            draggable={true}
                            onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)}
                        />
                    </Map>
                </div>
                <Row className="controller">
                    
                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }} >{t("Update")}</Button>
                       
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(GoogleApiWrapper({
    apiKey: ("AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE")
})(PositionTab))