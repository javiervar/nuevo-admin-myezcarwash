import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import {Polygon} from 'google-map-react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

import { Row, Col, Button } from 'react-bootstrap';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
const AnyReactComponent = ({ text }) => <div>{text}</div>;

class DrawingMapTab extends Component {
    constructor(props) {
        super(props)
        this.map = React.createRef();
        this.state = {
            coordinates: [],
            shape: null,
            center: {
                lat: 30.1229035,
                lng: -95.4306791,
            },
        }

    }

    static defaultProps = {

        zoom: 11
    };



    componentWillMount() {
        console.log("sup")

        if (navigator.geolocation) {
            console.log("entro")
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                this.setState({
                    center: pos
                })

            }, function (e) {
                console.log("error")
                console.log(e)
            });
        } else {
            // Browser doesn't support Geolocation
            console.log("Browser doesn't support Geolocation")
        }

    }

    handleGoogleMapApi(google) {
        const map = google.map
        const drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                ]
            },

        });
        drawingManager.setMap(map);
        //this.map=drawingManager;
        google.maps.event.addListener(drawingManager, 'overlaycomplete', (e) => {
            console.log(e)
            if (e.type == 'polygon') {
                var shape = e.overlay;
                this.setState({
                    shape: shape
                })
                var shapePath = e.overlay.getPath().getArray();
                var coordinates = [];
                for (var i = 0; i < shapePath.length; i++) {
                    var lat = shapePath[i].lat().toString();
                    var lng = shapePath[i].lng().toString();
                    var cors = { lat: lat, lng: lng }
                    coordinates.push(cors);
                }
                //console.log(coordinates);
                this.setState({
                    coordinates: coordinates
                })
            }
        });

    }

    erasePolygon = () => {
        console.log(this.map)
        this.state.shape.setMap(null)
    }

    handleNext = () => {
        this.props.handleData(this.state.coordinates);
    }

    render() {
        const{t}=this.props;
        const triangleCoords = [
            {lat: 25.774, lng: -80.190},
            {lat: 18.466, lng: -66.118},
            {lat: 32.321, lng: -64.757},
            {lat: 25.774, lng: -80.190}
          ];
        return (
            <div>
                <div className="title">
                    <h3>{t("Service area")}:</h3>
                </div>
                <div style={{ height: '400px', width: '100%' }}>
                    <Button size="sm" className={"btn-danger"} onClick={this.erasePolygon} style={{ position: 'absolute', margin: 5, zIndex: 1000 }}>
                        <span className="fas fa-times"></span>
                    </Button>
                    <GoogleMapReact
                        ref={this.map}
                        bootstrapURLKeys={{ key: "AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE", libraries: ['drawing'].join(',') }}
                        defaultCenter={this.state.center}
                        defaultZoom={this.props.zoom}
                        yesIWantToUseGoogleMapApiInternals
                        onGoogleApiLoaded={this.handleGoogleMapApi.bind(this)}
                    >
                       
                       

                    </GoogleMapReact>
                </div>
                <Row className="controller">

                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }}>{t("Update")}</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(DrawingMapTab);