import React from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import { Row, Col, Button } from 'react-bootstrap';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
export class MapPositionStep extends React.Component {
    state = {
        coordinates:{},
        currentLocation: {
            lat: 30.1229035,
            lng: -95.4306791,
        }
    }
    componentWillMount() {
        console.log("sup")

        if (navigator.geolocation) {
            console.log("entro")
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                console.log(pos)
                this.setState({
                    currentLocation: pos
                })

            }, function (e) {
                console.log("error")
                console.log(e)
            });
        } else {
            // Browser doesn't support Geolocation
            console.log("Browser doesn't support Geolocation")
        }

    }
    onMarkerDragEnd = (coords) => {
        console.log(coords)
        const { latLng } = coords;
        const lat = latLng.lat();
        const lng = latLng.lng();
        const coord = {
            lat: lat,
            lng: lng
        }
        console.log(coord)
        this.setState({
            coordinates:coord
        })
    }

    handleNext = () => {
        this.props.handleData(this.state.coordinates);
        this.props.nextStep();
    }

    render() {
        const{t}=this.props;
        return (
            <div>
                <div className="title">
                    <h3>{t("Location")} :</h3>
                </div>
                <h5>{t("Drag the marker to the center of the location.")}</h5>

                <div style={{ width: '100%', height: 400 }}>

                    <Map google={this.props.google} className="map" zoom={14} initialCenter={this.state.currentLocation} style={{ height: "400px", width: '90%' }}>
                        <Marker
                            name={'Current Location'}
                            position={this.state.currentLocation}
                            draggable={true}
                            onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)}
                        />
                    </Map>
                </div>
                <Row className="controller">
                    <Col>
                        <Button onClick={this.props.previousStep}>{t("Back")}</Button>
                    </Col>
                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }}>{t("Submit")}</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(GoogleApiWrapper({
    apiKey: ("AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE")
})(MapPositionStep))