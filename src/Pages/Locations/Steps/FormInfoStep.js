import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import Select from '../../Reports/Components/Select';
import '../Style/Style.css'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
const selecteData = {
    opc_service: [
        {
            name: 'Stationary',
            value: 1
        },
        {
            name: 'Mobile',
            value: 2
        }
    ],

};
class FormInfoStep extends React.Component {
    state = {
        serviceType:1,
        retailer:2,
        name:'',
        zip:'',
        tax:'',
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.handleData(this.state);
        this.props.nextStep()
    }

    handleServiceType = val => {
        console.log('servicetype ', val)
        this.setState({ serviceType: val })
    }
    handleRetailer = val => {
        console.log('retailer ', val)
        this.setState({ retailer: val })
    }
    render() {
        const{t}=this.props;
        return (
            <div>
                <div className="title">
                    <h3>{t("Information")}:</h3>
                </div>
                <Row>
                    <Col>
                        <Form onSubmit={this.handleSubmit}>
                            <Select label="Retailer:" options={this.props.retailers} onChange={this.handleRetailer} required={true}/>

                            <Form.Group size="sm">
                                <Form.Label>{t("Location Name")}: </Form.Label>
                                <Form.Control type="text" name="name"
                                    value={this.state.name}
                                    onChange={(e) => {
                                        this.setState(
                                            {
                                                name: e.target.value
                                            })
                                    }}
                                    required
                                />
                            </Form.Group>

                            <Form.Group size="sm">
                                <Form.Label>{t("ZIP")}: </Form.Label>
                                <Form.Control type="number" name="zip"
                                    value={this.state.zip}
                                    onChange={(e) => {
                                        this.setState(
                                            {
                                                zip: e.target.value
                                            })
                                    }}
                                    minLength={4}
                                    required
                                />
                            </Form.Group>

                            <Form.Group size="sm">
                                <Form.Label>{t("TAX")}: </Form.Label>
                                <Form.Control type="number" name="tax"
                                    value={this.state.tax}
                                    onChange={(e) => {
                                        this.setState(
                                            {
                                                tax: e.target.value
                                            })
                                    }}
                                    minLength={1}
                                    required
                                />
                            </Form.Group>

                            <Row className="controller">
                                <Col>
                                    <Button type="submit" style={{float:'right'}}>{t("Next")}</Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(FormInfoStep);