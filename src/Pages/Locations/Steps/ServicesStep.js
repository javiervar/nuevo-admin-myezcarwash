import React from 'react';
import { Row, Col, Card, Button, Accordion, Form } from 'react-bootstrap';
import Select from '../../Reports/Components/Select'
import '../Style/Style.css'
import Loading from "../../UIElements/Basic/Loading"
import { Link } from 'react-router-dom';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class ServicesStep extends React.Component {
    state = {}

    renderServices = () => {
        const{t}=this.props;
        const services = this.props.services.map((service,index) => {
            return (
                <Col key={index} style={{ marginTop: 5, flexDirection: 'row', alignContent: 'center', alignSelf: 'center' }}>
                    <Accordion >
                        <Card>
                            <Card.Header>
                                <h5>{service.SERVICE_NAME}
                                    <br></br>
                                    <span style={{color:'green'}}>${service.PRICE}</span></h5>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <span className="fas fa-sort-down"></span> {t("Details")}
                                </Accordion.Toggle>


                                <Form.Check
                                    style={{ position: 'absolute', top: 0, right: 0 }}
                                    type="checkbox"
                                    defaultChecked
                                    onClick={(e) => {
                                        this.changeServiceStatus(service);
                                    }
                                    } />

                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <h5>Description:</h5>
                                    <div style={{marginBottom:10}}>{service.DESCRIPTION}</div>
                                    <h5>Additionals:</h5>
                                    {service.ADDITIONALS.map((add) => {
                                        return (
                                            <Form.Group key={add.ADDITIONAL_ID} controlId="formBasicChecbox">
                                                <Form.Check
                                                    type="checkbox"
                                                    defaultChecked
                                                    label={add.ADDITIONAL_NAME + " $" + add.PRICE}
                                                    onClick={(e) => {
                                                        this.changeAddStatus(add);
                                                    }} 
                                                />
                                            </Form.Group>
                                        )
                                    }
                                    )}

                                </Card.Body>

                            </Accordion.Collapse>
                        </Card>

                    </Accordion>
                </Col>
            )
        });

        return services;
    }

    changeServiceStatus = (service) => {
        service.STATUS = service.STATUS == 1 ? 0 : 1;
    }
    changeAddStatus = (add) => {

        console.log("Add", add);
        add.STATUS = add.STATUS == 1 ? 0 : 1;

        console.log("cambio", this.props.services);
    }

    handleNext = () => {
        this.props.handleData();
        this.props.nextStep();
    }

    render() {
        const{t}=this.props;
        return (

            <div>
                <div className="title">
                    <h3>Services:</h3>
                </div>
                <Row className="justify-content-md-center">
                    {this.props.services.length==0?(
                        <Loading/>
                    ):
                    (this.renderServices())
                    }
                </Row>
                <Row className="controller">
                    <Col>
                        <Button onClick={this.props.previousStep}>{t("Back")}</Button>
                    </Col>
                    <Col>
                        <Button onClick={this.handleNext} style={{float:'right'}}>{t("Next")}</Button>
                    </Col>
                </Row>

            </div>
        );
    }
}

export default withNamespaces()(ServicesStep);