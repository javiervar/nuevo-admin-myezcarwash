import React from 'react';
import { Row, Col, Form, Button, Table } from 'react-bootstrap';
import '../Style/Style.css'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
class WorksDayStep extends React.Component {
    state = {
        sun: false,
        mon: false,
        tue: false,
        wed: false,
        thu: false,
        fri: false,
        sat: false,
    }

    handleNext = () => {
        this.props.handleData(this.state);
        this.props.nextStep();
    }

    render() {
        const{t}=this.props;
        return (
            <div>
                <div className="title">
                    <h3>{t("Works day")}:</h3>
                </div>

                <Row className="justify-content-md-center">
                    <Col xs={12} md={6}>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>{t("SUN")}</th>
                                    <th>{t("MON")} </th>
                                    <th>{t("TUE")}</th>
                                    <th>{t("WED")}</th>
                                    <th>{t("THU")}</th>
                                    <th>{t("FRI")}</th>
                                    <th>{t("SAT")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    sun: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    mon: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    tue: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    wed: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    thu: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    fri: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>
                                    <td>
                                        <Form.Check
                                            type={'checkbox'}

                                            onClick={(e) => {
                                                this.setState({
                                                    sat: e.target.checked
                                                })
                                            }}
                                        />
                                    </td>


                                </tr>
                            </tbody>
                        </Table>

                    </Col>
                </Row>
                <Row className="controller">
                    <Col>
                        <Button onClick={this.props.previousStep}>{t("Back")}</Button>
                    </Col>
                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }}>{t("Next")}</Button>
                    </Col>
                </Row>
            </div >
        );
    }
}

export default withNamespaces()(WorksDayStep);