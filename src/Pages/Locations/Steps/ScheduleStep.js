import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import Select from '../../Reports/Components/Select'
import '../Style/Style.css'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';
class ScheduleStep extends React.Component {
    state = {}

    renderHours = () => {
        const schedule = this.props.schedule.map((schedule,index) => {
            return (
                <Row key={index} style={{ marginTop: 5, flexDirection: 'row', alignContent: 'center', alignSelf: 'center' }}>
                    <Col md={6} xs={6} style={{ flexDirection: 'row', alignContent: 'center', alignSelf: 'center', textAlign: 'center' }}>
                        <b>{schedule.HOUR_12}</b>
                    </Col>
                    <Col md={6} xs={6}>
                        <Form.Control type="number" name="name"
                            defaultValue={2}
                            min={0}
                            id={'max' + schedule.SCHEDULE_ID}
                        />
                    </Col>
                </Row>
            )
        });

        return schedule;
    }

    handleNext = () => {
        const schedule = this.props.schedule.map((schedule) => {
            schedule.MAX = document.getElementById('max' + schedule.SCHEDULE_ID).value;
            return schedule;
        })

        this.props.handleData(schedule);
        this.props.nextStep();
    }

    render() {
        const{t}=this.props;
        return (
            <div>
                <div className="title">
                    <h3>{t("Time")}:</h3>
                </div>
                <Row>
                    <Col md={3} xs={12}>
                        <Select label="From:" options={this.props.options} onChange={this.props.handleFromTime} />
                    </Col>
                    <Col md={3} xs={12}>
                        <Select label="To:" defaultValue={18} options={this.props.options} onChange={this.props.handleToTime} />

                    </Col>

                </Row>
                <Row className="justify-content-md-center">
                    <Col xs={12} md={6}>
                        <Row style={{ marginBottom: 5 }}>
                            <Col md={6} xs={6} style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center', textAlign: 'center', background: '#4CAF50', padding: 5, color: "#fff" }}>
                                <b>{t("Hour")}</b>
                            </Col>
                            <Col md={6} xs={6} style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center', textAlign: 'center', background: '#4CAF50', padding: 5, color: "#fff" }}>
                                <b>Max</b>
                            </Col>
                        </Row>
                        {this.renderHours()}

                    </Col>
                </Row>
                <Row className="controller">
                    <Col>
                        <Button onClick={this.props.previousStep}>{t("Back")}</Button>
                    </Col>
                    <Col>
                        <Button onClick={this.handleNext} style={{ float: 'right' }}>{t("Next")}</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(ScheduleStep);