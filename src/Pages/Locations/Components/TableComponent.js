import React from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

class TableComponent extends React.Component {
    state = {}

    renderRows = () => {
        const{t}=this.props;
        const rows = this.props.data.map((row, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{row.LOCATION_ID}</th>
                    <td>{row.LOCATION_NAME}</td>
                    <td>{row.SERVICE_TYPE==1?'Stationary':'Mobile'}</td>
                    <td>{row.USER_NAME}</td>
                    <td>{row.ZIP}</td>
                    <td>{row.TAX}</td>
                    <td>{row.STATUS==1?'Active':'Disable'}</td>
                    <td>
                        {row.SERVICE_TYPE==1?(
                             <Link
                             className="btn btn-primary btn-sm"
                             to={{
                                 pathname: "/locations/editLocation",
                                 state: [{ locationId: row.LOCATION_ID }]
                             }}
                         >{t("Edit")}</Link>
                        ):(
                            <Link
                            className="btn btn-primary btn-sm"
                            to={{
                                pathname: "/locations/editMobile",
                                state: [{ locationId: row.LOCATION_ID }]
                            }}
                        >{t("Edit")}</Link>
                        )}
                       
                    </td>

                </tr>
            );
        })

        return rows


    }
    render() {
        const{t}=this.props
        return (
            <Table striped responsive bordered hover size="sm" id="table-to-xls">

                <thead key="thead">

                    <tr>
                        <th>Id</th>
                        <th>{t("Location Name")}</th>
                        <th>{t("Service type")}</th>
                        <th>{t("Retailer")}</th>
                        <th>{t("ZIP")}</th>
                        <th>{t("TAX")}</th>
                        <th>{t("Status")}</th>
                        <th>{t("Command")}</th>
                        
                    </tr>

                </thead>


                <tbody key="tbody">
                    {this.renderRows()}
                </tbody>
            </Table >
        );
    }
}

export default withNamespaces()(TableComponent);