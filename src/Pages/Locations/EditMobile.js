import React from 'react';
import { Row, Col, Card, Tabs, Tab, Button } from 'react-bootstrap';
import '../../assets/css/table.css'
import axios from 'axios';
import Aux from "../../hoc/_Aux";
import Loading from "../UIElements/Basic/Loading"
import { Link } from 'react-router-dom'
import CONSTANT from '../../store/constant';
import InfoTab from './Tabs/InfoTab';
import ServicesTab from './Tabs/ServicesTab';
import WorksDayTab from './Tabs/WorksDaysTab';
import ScheduleTab from './Tabs/ScheduleTab';
import EditDrawingMap from './EditDrawingMap';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
class EditMobile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            locationId: 0,
            retailers: [],
            info: {},
            user: {},
            retailerId: 2,
            services: [],
            worksdays: [],
            schedule: [],
            locationArea: [
                { lat: 25.774, lng: -80.190 },
                { lat: 18.466, lng: -66.118 },
                { lat: 32.321, lng: -64.757 },
                { lat: 25.774, lng: -80.190 }
            ],
            mapPosition: {
                lat: 30.1229035,
                lng: -95.4306791,
            }
        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user,
                locationId: this.props.location.state[0].locationId
            }, () => {
               
                this.getRetailers();
                
                
            })
        } else {
            this.props.history.push("/");
        }

    }


    getRetailers() {
        this.setState({ loading: true })
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;
        axios.get(CONSTANT.API_PATH + "/getRetailerItems")
            .then((response) => {
                this.setState({
                    retailers: response.data.data,
                },()=>{
                    this.getLocationInfo();
               
                })
            })
            .catch((error) => {
                console.log(error);
                
            })
    }
    getLocationInfo = () => {
        var postData = {
            params: {
                location: this.state.locationId,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getLocationInfo", postData)
            .then((response) => {
                var data = response.data.data[0];

                console.log(data)
                this.setState({
                    info: data,
                    retailerId: data.RETAILER_ID,
                },()=>{
                    
                    this.getLocationArea();
                })
                

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getLocationArea = () => {
        var postData = {
            params: {
                location: this.state.locationId,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getLocationArea", postData)
            .then((response) => {
                var data = response.data.data;
                data = data.map((cors) => {
                    return { lat: parseFloat(cors.lat), lng: parseFloat(cors.lng) }
                })

                console.log(data)
                this.setState({
                    locationArea: data,

                },()=>{
                    this.getLocationServices();
                    
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }



    getLocationServices = () => {
        var postData = {
            params: {
                location: this.state.locationId,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAbleServices", postData)
            .then((response) => {
                var data = response.data.data;
                console.log("services ", data)
                this.setState({
                    services: data,
                },()=>{
                    this.getLocationSchedule();
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getLocationSchedule = () => {
        var postData = {
            params: {
                location: this.state.locationId,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getScheduleLocation", postData)
            .then((response) => {
                var data = response.data.data;
                this.setState({
                    schedule: data,

                },()=>{
                    this.getWorksDays();
                })
                console.log("time", data)

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getWorksDays = () => {
        var postData = {
            params: {
                location: this.state.locationId,
            }
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getWorkDays", postData)
            .then((response) => {

                var data = response.data.data;
                var works = {
                    sun: false,
                    mon: false,
                    tue: false,
                    wed: false,
                    thu: false,
                    fri: false,
                    sat: false,
                }
                for (var day of data) {
                    switch (parseInt(day.DAY_ID)) {
                        case 0:
                            works.sun = true;
                            break;
                        case 1:
                            works.mon = true;
                            break;
                        case 2:
                            works.tue = true;
                            break;
                        case 3:
                            works.wed = true;
                            break;
                        case 4:
                            works.thu = true;
                            break;
                        case 5:
                            works.fri = true;
                            break;
                        case 6:
                            works.sat = true;
                            break;
                        default:

                    }
                }

                this.setState({
                    loading: false,
                    worksdays: works,

                })

            })
            .catch((error) => {
                console.log(error);
            })
    }

    handlePageClick = data => {
        let selected = data.selected;
        this.searchData(selected);
    };



    saveInfo = () => {
        this.setState({ loading: true });
        const { info } = this.state;
        const{t}=this.props;

        var params = info;
        console.log(params)
        params.serviceType = this.state.serviceType;
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/updateLocationInfo", params)
            .then((response) => {
                var data = response.data;
                this.setState({ loading: false });
                if (data.error == 0) {
                    alert(t('Data Changed Correctly!'));
                } else {
                    alert("Error")
                }

            });
    }

    //SAVE WORKS DAY
    saveWD = () => {
        const { worksdays } = this.state;
        var days = [];
        if (worksdays.sun) {
            days.push(0);
        }
        if (worksdays.mon) {
            days.push(1);
        }
        if (worksdays.tue) {
            days.push(2);
        }
        if (worksdays.wed) {
            days.push(3);
        }
        if (worksdays.thu) {
            days.push(4);
        }
        if (worksdays.fri) {
            days.push(5);
        }
        if (worksdays.sat) {
            days.push(6);
        }
        var index = days.length;

        this.setState({ loading: true });
        const { locationId } = this.state;
        var params = {
            LOCATION_ID: locationId
        };
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/deleteWorksDay", params)
            .then((response) => {
                var data = response.data;
                if (data.error == 0) {
                    this.saveWD2(locationId, days, index - 1);
                } else {
                    alert("Error")
                }

            });
    }
    saveWD2 = (locationId, days, index) => {
        const{t}=this.props;
        var params = {
            location: locationId,
            day: days[index],
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addWorkDay", params)
            .then((response) => {
                if (index > 0)
                    this.saveWD2(locationId, days, index - 1);
                else {
                    alert(t('Data Changed Correctly!'));
                    this.setState({ loading: false });

                }

            });
    }

    saveServices = () => {
        this.setState({ loading: true });
        const { services } = this.state;
        var index = services.length;
        this.saveServices2(services, index - 1);
    }

    saveServices2 = (services, index) => {
        const{t}=this.props;
        var params = services[index];

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/editServiceStatus", params)
            .then((response) => {
                var additionals = params.ADDITIONALS;
                var indexAdds = additionals.length;
                if (indexAdds > 0) {
                    this.saveServices2Adds(additionals, indexAdds - 1);
                }
                if (index > 0) {
                    this.saveServices2(services, index - 1);
                } else {
                    alert(t('Data Changed Correctly!'));
                    this.setState({
                        loading: false,
                    })
                }
            });
    }

    //recursivity save additional of services
    saveServices2Adds = (additionals, index) => {
        var params = additionals[index];
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/editAdditionalStatus", params)
            .then((response) => {
                if (index > 0) {
                    this.saveServices2Adds(additionals, index - 1);
                }
            });
    }


    saveSchedule = (data) => {
        this.setState({ loading: true });
        var index = data.length;
        this.saveSchedule2(data, index - 1);
    }
    saveSchedule2 = (data, index) => {
        const{t}=this.props;
        var params = data[index];
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/updateLocationHD", params)
            .then((response) => {
                if (index > 0) {
                    this.saveSchedule2(data, index - 1);
                } else {
                    alert(t('Data Changed Correctly!'));
                    this.setState({ loading: false });
                }
            });
    }
    //UPDATE LOCATION
    updateLocation = (data) => {
        const{t}=this.props;
        var params = {
            location: this.state.locationId,
            lat: data.lat,
            lng: data.lng
        }
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/updateCoordinates", params)
            .then((response) => {

                console.log(response);
                if (response.data.error == 0) {
                    alert(t('Data Changed Correctly!'));
                }

            });
    }
    //HANDLE UPDATE COORS OF SERVICE AREA
    updateLocation = (data) => {
        this.deleteArea(data);

    }
    //REMOVE THE PREVIOUS AREA
    deleteArea = (data) => {
        this.setState({loading:true})
        var params = {}
        params.location = this.state.locationId;

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/deleteLocationArea", params)
            .then((response) => {
                console.log(response);
                //IF THE RESULT IS ERROR=0 THEN INSERT THE NEW SERVICE AREA
                if (response.data.error == 0) {
                    console.log(data);
                    var index = data.length;
                    this.saveArea(data, index - 1);
                }

            });
    }

    //SAVE NEW SERVICE AREA
    saveArea = (coors, index) => {
        const{t}=this.props;
        var params = coors[index];
        if (index == 0) {
            var center = this.get_polygon_centroid(coors);
            console.log(center)
            params.latCenter = center.latCenter;
            params.lngCenter = center.lngCenter;
        }
        params.location = this.state.locationId;
        params.index = index;

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.post(CONSTANT.API_PATH + "/addLocationArea", params)
            .then((response) => {
                console.log(response);
                if (index > 0) {
                    this.saveArea(coors, index - 1);

                }
                else {
                    alert("Service area was updated successfully");
                    this.setState({
                        loading: false,
                    },()=>{
                        this.getLocationArea();
                    })

                    
                }

            });
    }

    get_polygon_centroid = (pts) => {
        var first = pts[0], last = pts[pts.length - 1];
        if (first.lat != last.lat || first.lng != last.lng) pts.push(first);
        var twicearea = 0,
            x = 0, y = 0,
            nPts = pts.length,
            p1, p2, f;
        for (var i = 0, j = nPts - 1; i < nPts; j = i++) {
            p1 = pts[i]; p2 = pts[j];
            f = parseFloat(p1.lat) * parseFloat(p2.lng) - parseFloat(p2.lat) * parseFloat(p1.lng);
            twicearea += f;
            x += (parseFloat(p1.lat) + parseFloat(p2.lat)) * f;
            y += (parseFloat(p1.lng) + parseFloat(p2.lng)) * f;
        }
        f = twicearea * 3;
        return { latCenter: x / f, lngCenter: y / f };
    }

    render() {
        const{t}=this.props;
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>

                            <Card.Body>
                                {this.state.loading ? (
                                    <Loading />
                                ) : (
                                        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
                                            <Tab eventKey="home" title={t("Information")} >
                                                <InfoTab handleData={this.saveInfo} info={this.state.info} retailers={this.state.retailers} retailer={this.state.retailerId} />
                                            </Tab>
                                            <Tab eventKey="services" title={t("Services")}>
                                                <ServicesTab handleData={this.saveServices} services={this.state.services} />
                                            </Tab>
                                            <Tab eventKey="worksdays" title={t("Works days")}>
                                                <WorksDayTab handleData={this.saveWD} days={this.state.worksdays} />
                                            </Tab>
                                            <Tab eventKey="shedule" title={t("Shedule")}>
                                                <ScheduleTab handleData={this.saveSchedule} schedule={this.state.schedule} />
                                            </Tab>
                                            <Tab eventKey="map" title={t("Service area")}>
                                                <EditDrawingMap handleData={this.updateLocation} locationArea={this.state.locationArea} locationId={this.state.locationId} user={this.state.user} />
                                            </Tab>

                                        </Tabs>
                                    )}

                            </Card.Body>
                        </Card>


                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(EditMobile);