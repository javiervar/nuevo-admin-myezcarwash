import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { Row, Col, Button,Modal } from 'react-bootstrap';
import axios from 'axios';
import CONSTANT from '../../store/constant';

import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class EditDrawingMap extends Component {
    constructor(props) {
        super(props)
        this.map = React.createRef();
        this.state = {
            loading: false,
            coordinates: [],
            shape: null,
            locationId: -1,
            showModal:false,
            center: {
                lat: 30.1229035,
                lng: -95.4306791,
            },
        }

    }

    static defaultProps = {
        zoom: 11
    };


    componentWillMount() {
        console.log("sup")
        this.setState({
            locationId: this.props.locationId
        })
        if (navigator.geolocation) {
            console.log("entro")
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                this.setState({
                    center: pos
                })

            }, function (e) {
                console.log("error")
                console.log(e)
            });
        } else {
            // Browser doesn't support Geolocation
            console.log("Browser doesn't support Geolocation")
        }

    }

    handleGoogleMapApi(google) {
        const map = google.map
        const drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                ]
            },

        });
        drawingManager.setMap(map);

        var flightPlanCoordinates = [
            { lat: 37.772, lng: -122.214 },
            { lat: 21.291, lng: -157.821 },
            { lat: -18.142, lng: 178.431 },
            { lat: -27.467, lng: 153.027 }
        ];

        var flightPath = new google.maps.Polygon({
            path: this.props.locationArea,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        flightPath.setMap(map);


        //this.map=drawingManager;
        google.maps.event.addListener(drawingManager, 'overlaycomplete', (e) => {
            console.log(e)
            if (e.type == 'polygon') {
                var shape = e.overlay;
                this.setState({
                    shape: shape
                })
                var shapePath = e.overlay.getPath().getArray();
                var coordinates = [];
                for (var i = 0; i < shapePath.length; i++) {
                    var lat = shapePath[i].lat().toString();
                    var lng = shapePath[i].lng().toString();
                    var cors = { lat: lat, lng: lng }
                    coordinates.push(cors);
                }
                //console.log(coordinates);
                this.setState({
                    coordinates: coordinates
                })
            }
        });

    }

    erasePolygon = () => {
        if (this.state.shape == null) {
            alert("Please draw a valid area");
            return;
        }
        this.state.shape.setMap(null)
    }

    validNext=()=>{
        if (this.state.shape == null) {
            alert("Please draw a valid area");
            return;
        }
        this.setState({
            showModal:true
        })
    }

    handleNext = () => {
        this.props.handleData(this.state.coordinates);
    }


    handleClose = () => this.setState({showModal:false});


    render() {
        const{t}=this.props;
        return (

            <div>
                <Modal show={this.state.showModal} onHide={this.handleClose} size="sm">
                    <Modal.Header closeButton>
                        <Modal.Title>{t("Are you sure you update the service area?")}</Modal.Title>
                    </Modal.Header>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            No
                        </Button>
                        <Button variant="primary" onClick={this.handleNext}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>
                <div className="title">
                    <h3>{t("Edit service area:")}</h3>
                </div>
                <div style={{ height: '400px', width: '100%' }}>
                    <Button size="sm" className={"btn-danger"} onClick={this.erasePolygon} style={{ position: 'absolute', margin: 5, zIndex: 1000 }}>
                        <span className="fas fa-times"></span>
                    </Button>
                    <GoogleMapReact
                        ref={this.map}
                        bootstrapURLKeys={{ key: "AIzaSyAFwCAm3xoeTkyUjd3Y1DLvdVVwmnsDOQE", libraries: ['drawing'].join(',') }}
                        defaultCenter={this.state.center}
                        defaultZoom={this.props.zoom}
                        yesIWantToUseGoogleMapApiInternals
                        onGoogleApiLoaded={this.handleGoogleMapApi.bind(this)}
                    >

                    </GoogleMapReact>
                </div>
                <Row className="controller">
                    <Col>
                        <Button onClick={this.validNext} style={{ float: 'right' }}>{t("Update")}</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withNamespaces()(EditDrawingMap);