import React from 'react';
import { Card } from 'react-bootstrap';
import CONSTANT from "../../../store/constant";


class GeneralInfoCard extends React.Component {
    state = {}


    render() {
        const { last, now } = this.props;
        var porcent = (now * 100 / last).toFixed(2);
        var diff=Math.abs(porcent-100);



        return (
            <div>

                <h6 className='mb-4'>{this.props.title}</h6>
                <div className="row d-flex align-items-center">
                    <div className="col-12">
                        <h3 className="f-w-300 d-flex align-items-center m-b-0">
                            {this.props.total > this.props.last ?
                                (<i className="feather icon-arrow-up text-c-green f-30 m-r-5" />) :
                                (<i className="feather icon-arrow-down text-c-red f-30 m-r-5" />)}
                            {this.props.symbol}{this.props.now}
                        </h3>
                    </div>

                    <div className="col-12" style={{marginTop:5,textAlign:'center'}}> 
                        <p className="m-b-0">{diff}% 
                        {this.props.total > this.props.last ?" more":" less"} than last {this.props.by}</p>
                    </div>
                </div>
                <div className="progress m-t-30" style={{ height: '7px' }}>
                    <div className="progress-bar progress-c-theme" role="progressbar" style={{ width: porcent+'%' }} aria-valuenow={porcent} aria-valuemin="0" aria-valuemax="100" />
                </div>

            </div>
        );
    }
}

export default GeneralInfoCard;