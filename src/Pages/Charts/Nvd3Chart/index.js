import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

import Aux from "../../../hoc/_Aux/index";
import LineChart from "./LineChart";
import BarDiscreteChart from "./BarDiscreteChart";
import BarChartComponent from "../Components/BarChartComponent";
import LineChartComponent from "../Components/LineChartComponent";

import GeneralInfoCard from "../Components/GeneralInfoCard";
import Select from "../../Reports/Components/Select";
import InlineSelect from "../../UIElements/Basic/InlineSelect"

import PieDonutChart from "./PieDonutChart";
import Loading from "../../UIElements/Basic/Loading"

import axios from 'axios';
import CONSTANT from '../../../store/constant';
import i18n from '../../../i18n';
import { withNamespaces } from 'react-i18next';

const selectData = {
    opc_service: [
        {
            name: 'Stationary',
            value: 1
        },
        {
            name: 'Mobile',
            value: 2
        }
    ],
    opc_years: [
        {
            name: '2020',
            value: 2020
        },
        {
            name: '2019',
            value: 2019
        },
        {
            name: "2018",
            value: 2018
        }
    ],
    opc_data: [
        {
            name: 'Earnings',
            value: 1
        },
        {
            name: 'Reservations',
            value: 2
        },

    ],
    opc_top: [
        {
            name: 'Year',
            value: 1
        },
        {
            name: 'Month',
            value: 2
        },

    ]
}
class Nvd3Chart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            loadingMS: true,
            loadingWS: true,
            loadingLA: true,
            loadingMR: true,
            loadingME: true,
            loadingAddsA: true,
            loadingSerA: true,
            loadingEmpA: true,
            weeklySales: [],
            monthlySales: [],
            maxMonthlyValue:10000,
            monthlyStats:[],
            allLocationsSales: [],
            averageReservations: [],
            monthlyReservations: [],
            reservationTotal: 0,
            lastReservations: 0,
            averageService: [],
            averageAdds: [],
            averageEmploye: [],
            retailerItems: [],
            actualMonthE: 0,
            lastMonthE: 0,
            year: 2020,
            type: 1,
            retailer: -1

        }
    }

    componentWillMount() {
        if (localStorage.getItem("adminwash") !== null) {
            var user = JSON.parse(localStorage.getItem("adminwash"));
            this.setState({
                user: user
            }, () => {
                this.initData();
                this.getRetailersItems();
                this.getMonthComparation();

            })
            console.log(user)
        } else {
            this.props.history.push("/");
        }

    }
    initData = () => {
        this.getMonthlySales();
        //this.getMonthlyReservations();
        this.getWeeklySales();
        this.getAverageReservationsByLocation(1);
        this.getServiceAverage(1);
        this.getAddsAverage(1);
        this.getEmployeAverage(1);
    }

    getRetailersItems = () => {

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getRetailerItems")
            .then((response) => {
                console.log(response);
                response.data.data.unshift({ name: 'All', value: -1 })
                this.setState({
                    retailerItems: response.data.data,
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getMonthlySales = () => {
        this.setState({
            loadingMS: true,
            monthlyStats:[]
        })
        var postData = {
            params: {
                year: this.state.year,
                retailer: this.state.retailer,
                type: this.state.type,

            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getMontlySales", postData)
            .then((response) => {
                console.log(response);
                var monthly = response.data;
                var max=0;
                for(var m of monthly){
                    if(max<m.amount){
                        max=m.amount
                    }
                }
                console.log(max)
                this.setState({
                    monthlyStats: monthly,
                    maxMonthlyValue:Math.round(max)+1000,
                    loadingMS: false

                })

            })
            .catch((error) => {
                console.log(error);

            });
    }
    getMonthlyReservations = () => {
        this.setState({
            loading: true,
            monthlyStats:[]
        })
        var postData = {
            params: {
                year: this.state.year,
                retailer: this.state.retailer
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getMontlyReservations", postData)
            .then((response) => {
                console.log(response);
                var monthly = response.data;
                var max=0;
                for(var m of monthly){
                    if(max<m.amount){
                        max=m.amount
                    }
                }
                this.setState({
                    monthlyStats: monthly,
                    maxMonthlyValue:Math.round(max)+100,

                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getMonthComparation = () => {
        //loading monthly reservation and earnings
        this.setState({
            loadingME: true,
            loadingMR: true
        })

        var postData = {
            params: {
                year: this.state.year,
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getLastMonthEarningsComparation", postData)
            .then((response) => {
                console.log(response);
                var data = response.data;

                this.setState({
                    actualMonthE: data[0].amount,
                    lastMonthE: data[1].amount,
                    loadingME: false
                })

            })
            .catch((error) => {
                console.log(error);

            });

        axios.get(CONSTANT.API_PATH + "/getLastMonthReservationsComparation", postData)
            .then((response) => {
                console.log(response);
                var data = response.data;

                this.setState({
                    reservationTotal: data[0].amount,
                    lastReservations: data[1].amount,
                    loadingMR: false
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getWeeklySalesByLocations = () => {
        this.setState({
            loading: true
        })
        var postData = {
            params: {
                year: this.state.year,
                type: this.state.type,
                retailer: this.state.retailer
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getWeeklySalesByLocation", postData)
            .then((response) => {
                console.log(response);
                for (var val of response.data) {
                    val.key = val.name
                }
                this.setState({
                    loading: false,
                    allLocationsSales: response.data
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getAverageReservationsByLocation = (opt) => {
        this.setState({
            loadingLA: true,
            averageReservations: []
        })
        var postData = {
            params: {
                year: this.state.year,
                type: this.state.type,
                retailer: this.state.retailer,
                option: opt,

            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getReservationAverage", postData)
            .then((response) => {
                console.log(response);

                this.setState({
                    loadingLA: false,
                    averageReservations: response.data
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getServiceAverage = (opt) => {
        this.setState({
            loadingSerA: true,
            averageService: []
        })
        var postData = {
            params: {
                year: this.state.year,
                type: this.state.type,
                retailer: this.state.retailer,
                option: opt
            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getServiceAverage", postData)
            .then((response) => {
                console.log(response);

                this.setState({
                    averageService: response.data,
                    loadingSerA: false
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getEmployeAverage = (opt) => {
        this.setState({
            loadingEmpA: true,
            averageEmploye: []
        })
        var postData = {
            params: {
                year: this.state.year,
                type: this.state.type,
                retailer: this.state.retailer,
                option: opt

            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getEmployeeAverage", postData)
            .then((response) => {
                console.log(response);

                this.setState({
                    averageEmploye: response.data,
                    loadingEmpA: false
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    getAddsAverage = (opt) => {
        this.setState({
            loadingAddsA: true,
            averageAdds: []
        })
        var postData = {
            params: {
                year: this.state.year,
                type: this.state.type,
                retailer: this.state.retailer,
                option: opt

            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getAdditionalAverage", postData)
            .then((response) => {
                console.log(response);

                this.setState({
                    averageAdds: response.data,
                    loadingAddsA: false
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }



    getWeeklySales = () => {
        this.setState({
            loadingWS: true
        })
        var postData = {
            params: {
                year: this.state.year,
                retailer: this.state.retailer,
                type: this.state.type

            }
        };
        console.log(postData);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = this.state.user.token;

        axios.get(CONSTANT.API_PATH + "/getWeeklySales", postData)
            .then((response) => {
                console.log(response);

                this.setState({
                    weeklySales: response.data,
                    loadingWS: false
                })

            })
            .catch((error) => {
                console.log(error);

            });
    }

    //Filters
    handleYear = (year) => {
        console.log(year);
        this.setState({
            year: year,
            weeklySales: [],
            monthlyStats: [],
            allLocationsSales: [],
            averageReservations: [],
            monthlyReservations: [],
            averageService: [],
            averageAdds: [],
        }, () => {
            this.initData();
        })
    }
    handleType = (type) => {
        console.log(type);
        this.setState({
            type: type,
            weeklySales: [],
            monthlyStats: [],
            allLocationsSales: [],
            averageReservations: [],
            monthlyReservations: [],
            averageService: [],
            averageAdds: [],
        }, () => {
            this.initData();
        })
    }
    handleRetailer = (id) => {
        console.log(id);
        this.setState({
            retailer: id,
            weeklySales: [],
            monthlyStats: [],
            allLocationsSales: [],
            averageReservations: [],

            monthlyReservations: [],

            averageService: [],
            averageAdds: [],
        }, () => {
            this.initData();
        })
    }
    handleTopEmployee = (opc) => {
        this.getEmployeAverage(opc);
    }
    handleTopLocation = (opc) => {
        this.getAverageReservationsByLocation(opc);
    }
    handleTopService = (opc) => {
        this.getServiceAverage(opc);
    }
    handleTopAdds = (opc) => {
        this.getAddsAverage(opc);
    }
    handleMonthlyStats=(opc)=>{
        if(opc==1){
            this.getMonthlySales();
        }else{
            this.getMonthlyReservations();
        }
    }
    render() {
        const { t } = this.props;

        return (
            <Aux>
                <Row>
                    <Col xs={6}>
                        <Card>
                            <Card.Body>

                                {this.state.loadingME ? (
                                    <Loading />
                                ) : (
                                        <GeneralInfoCard title={t("Earnings")} now={this.state.actualMonthE} last={this.state.lastMonthE} symbol={"$"} by={"month"} />
                                    )}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6}>
                        <Card>
                            <Card.Body>

                                {this.state.loadingMR ? (
                                    <Loading />
                                ) : (
                                        <GeneralInfoCard title={t("Reservations")} now={this.state.reservationTotal} last={this.state.lastReservations} symbol={""} by={"month"} />
                                    )}
                            </Card.Body>
                        </Card>

                    </Col>

                </Row>
                {/**************** FILTERS ***************/}
                <Row>
                    <Col md={12}>
                        <Card>
                            <Card.Body className="text-center">
                                <Row>
                                    <Col md="3">
                                        <Select label={t("Year")} options={selectData.opc_years} onChange={this.handleYear} />
                                    </Col>
                                    <Col md="3">
                                        <Select label={t("Service type")} options={selectData.opc_service} onChange={this.handleType} />
                                    </Col>
                                    <Col md="3">
                                        <Select label={t("Retailer")} options={this.state.retailerItems} onChange={this.handleRetailer} />
                                    </Col>



                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row>


                    <Col md={6} xs={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Top location")}</Card.Title>
                            </Card.Header>
                            <Card.Body className="text-center">
                                <Row>
                                    <Col md={6}>
                                        <InlineSelect label={t("By")} options={selectData.opc_top} onChange={this.handleTopLocation} />
                                    </Col>
                                </Row>
                                {this.state.loadingLA ? (
                                    <Loading />
                                ) : (this.state.averageReservations.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <PieDonutChart data={this.state.averageReservations} />
                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xs={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Top Employee")}</Card.Title>
                            </Card.Header>
                            <Card.Body className="text-center">
                                <Row>
                                    <Col md={6}>
                                        <InlineSelect label={t("By")} options={selectData.opc_top} onChange={this.handleTopEmployee} />
                                    </Col>
                                </Row>
                                {this.state.loadingEmpA ? (
                                    <Loading />
                                ) : (this.state.averageEmploye.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <PieDonutChart data={this.state.averageEmploye} />
                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xs={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Top service")}</Card.Title>
                            </Card.Header>
                            <Card.Body className="text-center">
                                <Row>
                                    <Col md={6}>
                                        <InlineSelect label={t("By")} options={selectData.opc_top} onChange={this.handleTopService} />
                                    </Col>
                                </Row>
                                {this.state.loadingSerA ? (
                                    <Loading />
                                ) : (this.state.averageService.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <PieDonutChart data={this.state.averageService} />
                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col md={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Top Additional")}</Card.Title>
                            </Card.Header>
                            <Card.Body className="text-center">
                                <Row>
                                    <Col md={6}>
                                        <InlineSelect label={t("By")} options={selectData.opc_top} onChange={this.handleTopAdds} />
                                    </Col>
                                </Row>
                                {this.state.loadingAddsA ? (
                                    <Loading />
                                ) : (this.state.averageAdds.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <PieDonutChart data={this.state.averageAdds} />
                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Montly Stats")}</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={6}>
                                        <InlineSelect label={t("By")} options={selectData.opc_data} onChange={this.handleMonthlyStats} />
                                    </Col>
                                </Row>
                                {this.state.loadingMS ? (
                                    <Loading />
                                ) : (this.state.monthlyStats.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <div style={{ width: "100%", height: 300 }}>
                                            <BarChartComponent data={this.state.monthlyStats} maxValue={this.state.maxMonthlyValue}/>
                                        </div>

                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{t("Weekly Earnings")}</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                {this.state.loadingWS ? (
                                    <Loading />
                                ) : (this.state.weeklySales.length == 0 ? (
                                    <h5>{t("No results")}</h5>
                                ) : (
                                        <div style={{ width: "100%", height: 300 }}>
                                            <LineChartComponent data={this.state.weeklySales} />
                                        </div>

                                    ))}
                            </Card.Body>
                        </Card>
                    </Col>

                </Row>
            </Aux>
        );
    }
}

export default withNamespaces()(Nvd3Chart);